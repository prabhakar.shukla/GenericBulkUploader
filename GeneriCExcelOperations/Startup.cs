﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GeneriCExcelOperations.Startup))]
namespace GeneriCExcelOperations
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
