﻿using System;
using System.Collections.Generic;
using System.Web;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using System.IO;
using System.Data;
using System.Threading.Tasks;
using System.Text;
using System.Globalization;
using System.Diagnostics;
using System.Linq;


/// <summary>
/// This class is used for creating excel file and setting excel file.
/// </summary>
public static class SpreadsheetLightCreatExcelFile
{

    public static bool CreateExcelDocument(DataSet ds, string excelFilename,string Headerstyle,DataSet dsStyle = null,bool Freeze = false,int FreezeColumnNo = 0)
    {
        SLDocument document = new SLDocument();
        try
        {
           
            WriteExcelFile(ds, ref document, Headerstyle, Freeze, FreezeColumnNo);
            document.SaveAs(excelFilename);
            WriteStyleExcelFile(dsStyle, excelFilename);
            Trace.WriteLine("Successfully created: " + excelFilename);
            return true;
        }
        catch (Exception ex)
        {
            Trace.WriteLine("Failed, exception thrown: " + ex.Message);
            return false;
        }
        finally
        {
            document.Dispose();
        }
    }

    private static void WriteExcelFile(DataSet ds, ref SLDocument spreadsheet, string Headerstyle,bool Freeze,int FreezeColumnNo)
    {
        int worksheetcounter = 0;
        foreach (DataTable dt in ds.Tables)
        {
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                               .Select(x => x.ColumnName)
                               .ToArray();
            if (worksheetcounter == 0)
            {
                spreadsheet.RenameWorksheet(SLDocument.DefaultFirstSheetName, dt.TableName);
            }
            else
            {
                spreadsheet.AddWorksheet(dt.TableName);
            }

            CreateWorkSheetHeader(columnNames, ref spreadsheet);
            SetCellvalue(dt, ref  spreadsheet, 1, 1);
            if(Freeze == true)
            {
                spreadsheet.FreezePanes(1, FreezeColumnNo);
            }
            spreadsheet.SetRowStyle(1, SetStyleSheetCell(ref  spreadsheet, Headerstyle));
            worksheetcounter++;
        }
    }

    private static void WriteStyleExcelFile(DataSet ds, string excelFilename)
    {
        foreach (DataTable dt in ds.Tables)
        {
            WriteSetCellStyle(excelFilename, dt.TableName, dt);
        }
    }

    private static void WriteExcelFile(DataTable dt, ref SLDocument spreadsheet)
    {
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                           .Select(x => x.ColumnName)
                           .ToArray();
  

        spreadsheet.RenameWorksheet(SLDocument.DefaultFirstSheetName, dt.TableName);
        CreateWorkSheetHeader(columnNames, ref spreadsheet);
        SetCellvalue(dt, ref  spreadsheet, 1, 0);
    }

    static void CreateWorkSheetHeader(string[] columnNames, ref SLDocument spreadsheet)
    {
        for (int h = 0; h <= columnNames.Length - 1; h++)
        {
            spreadsheet.SetCellValue(1, h + 1, columnNames[h].ToString());
        }
    }


    static void SetCellvalue(DataTable dt, ref SLDocument sl, int StartRow, int InitialStartRow = 0)
    {
        for (int i = 0; i <= dt.Rows.Count - 1; i++)
        {
            for (int j = 0; j <= dt.Columns.Count - 1; j++)
            {
                var strdatatype = (dt.Rows[i][j].GetType());
                if (strdatatype.FullName == "System.String")
                {
                    sl.SetCellValue(InitialStartRow + StartRow, j + 1, dt.Rows[i][j].ToString());
                }
                else if (strdatatype.FullName == "System.Decimal")
                {
                    sl.SetCellValue(InitialStartRow + StartRow, j + 1, Convert.ToDecimal(dt.Rows[i][j]));
                }
                else if (strdatatype.FullName == "System.Double")
                {
                    sl.SetCellValue(InitialStartRow + StartRow, j + 1, Convert.ToDouble(dt.Rows[i][j]));
                }
                else if (strdatatype.FullName == "System.ToInt64")
                {
                    sl.SetCellValue(InitialStartRow + StartRow, j + 1, Convert.ToInt64(dt.Rows[i][j]));
                }
                else
                {
                    try
                    {
                        if (dt.Rows[i][j].ToString() != "")
                            sl.SetCellValue(InitialStartRow + StartRow, j + 1, Convert.ToInt32(dt.Rows[i][j]));
                        else
                            sl.SetCellValue(InitialStartRow + StartRow, j + 1, 0);
                    }
                    catch(Exception ex)
                    {
                        if (dt.Rows[i][j].ToString() != "")
                            sl.SetCellValue(InitialStartRow + StartRow, j + 1, Convert.ToString(dt.Rows[i][j]));
                        else
                            sl.SetCellValue(InitialStartRow + StartRow, j + 1, 0);
                    }
                }
            }

            InitialStartRow++;
        }


    }

    static void WriteSetCellStyle(string Path, string SheetName, DataTable dt)
    {
        SLDocument sl = new SLDocument(Path, SheetName);
        using (sl)
        {
            SLStyle style4 = sl.CreateStyle();
            style4.SetFontColor(System.Drawing.Color.Red);
            style4.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Yellow, System.Drawing.Color.Yellow);

            Parallel.For(0, dt.Rows.Count, new ParallelOptions { MaxDegreeOfParallelism = 1 }, i =>
            {
                sl.SetCellStyle(Convert.ToInt32(dt.Rows[i]["RowNo"]) + 1, Convert.ToInt32(dt.Rows[i]["ColumnNo"]), SetStyleSheetCell(ref  sl, Convert.ToString(dt.Rows[i]["stylename"])));
            }); // Par

            sl.SaveAs(Path);
        }
    }
    static SLStyle SetStyleSheetCell(ref SLDocument sl, string Stylename)
    {
        SLStyle style = sl.CreateStyle();
        if (Stylename == "Red")
        {
            style.SetFontColor(System.Drawing.Color.Red);
            style.Border.LeftBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.LeftBorder.Color = System.Drawing.Color.Black;
            style.Border.RightBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.RightBorder.Color = System.Drawing.Color.Black;
            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.BottomBorder.Color = System.Drawing.Color.Black;
            style.Border.TopBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.TopBorder.Color = System.Drawing.Color.Black;
            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Red, System.Drawing.Color.Red);
        }
        else if (Stylename == "Yellow")
        {
            style.SetFontColor(System.Drawing.Color.Yellow);
            style.Border.LeftBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.LeftBorder.Color = System.Drawing.Color.Black;
            style.Border.RightBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.RightBorder.Color = System.Drawing.Color.Black;
            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.BottomBorder.Color = System.Drawing.Color.Black;
            style.Border.TopBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.TopBorder.Color = System.Drawing.Color.Black;
            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Yellow, System.Drawing.Color.Yellow);
        }
        else if (Stylename == "LightGreen")
        {
            style.SetFontColor(System.Drawing.Color.LightGreen);
            style.Border.LeftBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.LeftBorder.Color = System.Drawing.Color.Black;
            style.Border.RightBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.RightBorder.Color = System.Drawing.Color.Black;
            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.BottomBorder.Color = System.Drawing.Color.Black;
            style.Border.TopBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.TopBorder.Color = System.Drawing.Color.Black;
            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightGreen, System.Drawing.Color.LightGreen);
        }
        else if (Stylename == "Header")
        {
            style.SetFontColor(System.Drawing.Color.Black);
            style.SetFontBold(true);
            style.Border.LeftBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.LeftBorder.Color = System.Drawing.Color.Black;
            style.Border.RightBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.RightBorder.Color = System.Drawing.Color.Black;
            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.BottomBorder.Color = System.Drawing.Color.Black;
            style.Border.TopBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.TopBorder.Color = System.Drawing.Color.Black;
            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightBlue, System.Drawing.Color.LightBlue);
        }


        else if (Stylename == "HeaderGreen")
        {
            style.SetFontColor(System.Drawing.Color.Black);

            style.Border.LeftBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.LeftBorder.Color = System.Drawing.Color.Black;
            style.Border.RightBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.RightBorder.Color = System.Drawing.Color.Black;
            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.BottomBorder.Color = System.Drawing.Color.Black;
            style.Border.TopBorder.BorderStyle = BorderStyleValues.Thin;
            style.Border.TopBorder.Color = System.Drawing.Color.Black;
            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Green, System.Drawing.Color.LightBlue);
        }

  else if (Stylename == "checkval")
        {
            
            style.FormatCode = "#,##0;\"0\"";
        }

        return style;
    }
}