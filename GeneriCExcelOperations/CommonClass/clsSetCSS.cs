﻿using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data;

namespace GeneriCExcelOperations.CommonClass
{
    /// <summary>
    /// This clas is used for making style sheet in excel file.
    /// </summary>
    public class clsSetCSS
    {
        /// <summary>
        /// This method is used for high lighting error in excel file with a colour.
        /// </summary>
        /// <param name="Path">Excel file path.</param>
        /// <param name="SheetName">Sheet name of excel file .</param>
        /// <param name="ErrorTable">This table contains error records.</param>
        /// <param name="tableName">Tbale Name</param>
        public void SetErrorStyleSheet(string Path, string SheetName, System.Data.DataTable ErrorTable, string tableName)
        {
          
                SLDataValidation dv;
                using (SLDocument sl = new SLDocument(Path, SheetName))
                {
                    SLStyle style4 = sl.CreateStyle();
                    style4.SetFontColor(System.Drawing.Color.Red);
                    style4.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Yellow, System.Drawing.Color.Yellow);




                    SLStyle styleheader = sl.CreateStyle();
                    styleheader.SetFontColor(System.Drawing.Color.Black);
                    //styleheader.SetFontBold(true);
                    styleheader.Protection.Locked = false;
                    styleheader.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightBlue, System.Drawing.Color.LightBlue);
                    if (tableName == "upld_PointsUploadedDetails")
                    {


                        dv = sl.CreateDataValidation("E1", "F1");
                        dv.SetInputMessage("Rule", "Should be in proper date time format(MM/DD/YYYY).");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "SD field will accept string values upto 8 characters");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "  Batch field will accept alphanumeric values upto 10 characters with special characters i.e. '_' and  '/' included");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("G1", "G1");
                        dv.SetInputMessage("Rule", "Year field will be accept numeric[4]");
                        sl.AddDataValidation(dv);

                      
                        dv = sl.CreateDataValidation("J1", "J1");
                        dv.SetInputMessage("Rule", "Retailer ID is numeric[9]");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("K1", "K1");
                        dv.SetInputMessage("Rule", " Points uploaded is numeric[17]");
                        sl.AddDataValidation(dv);


                        dv = sl.CreateDataValidation("B1", "B1");
                        dv.SetInputMessage("Rule", " Record ID will be 10 digit code, auto-generated as: created date (DDMMYY) + serial number starting from 0001 when ACTION is not A.");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("A1", "A1");
                        dv.SetInputMessage("Rule", "Insert Only 'A','M','D' Other Wise this record will not be inserted.");
                        sl.AddDataValidation(dv);

                    }
                    else if (tableName == "upld_redemption")
                    {
                        dv = sl.CreateDataValidation("B1", "B1");
                        dv.SetInputMessage("Rule", " Record ID will be 10 digit code, auto-generated as: created date (DDMMYY) + serial number starting from 0001 when ACTION is not A.");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("A1", "A1");
                        dv.SetInputMessage("Rule", "Insert Only 'A','M','D' Other Wise this record will not be inserted.");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "Retailer ID field will accept  values upto 9 digit");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "Card field will accept  values upto 16 digit");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("E1", "E1");
                        dv.SetInputMessage("Rule", "Allocated Point field will accept  values upto 15 digit");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("F1", "F1");
                        dv.SetInputMessage("Rule", "Order At Place field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("G1", "G1");
                        dv.SetInputMessage("Rule", "Categery field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);
                    }
                    else if (tableName == "upld_PointExpiry")
                    {
                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "Retailer ID field will accept  values upto 9 digit");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "Date should be MM/DD/YYYY format");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("E1", "E1");
                        dv.SetInputMessage("Rule", "Point field will accept  values upto 15 digit");
                        sl.AddDataValidation(dv);
                    }
                    else if (tableName == "upld_RetailerSegmentation")
                    {
                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "Retailer ID field will accept  values upto 9 digit");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "YEAR should be YYYY format and also nummeric type.");
                        sl.AddDataValidation(dv);

                    }
                    else if (tableName == "upld_CategoryRedemption")
                    {
                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "Card field will accept  values upto 16 digit");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("E1", "E1");
                        dv.SetInputMessage("Rule", "ACTIVITY_ORDER_DATE should be MM/DD/YYYY format");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("G1", "G1");
                        dv.SetInputMessage("Rule", " ALLOCATED_POINTS uploaded is numeric[17]");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("H1", "H1");
                        dv.SetInputMessage("Rule", "Order At Place field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("I1", "I1");
                        dv.SetInputMessage("Rule", "Categery field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("J1", "J1");
                        dv.SetInputMessage("Rule", "Sub Categery field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);


                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "Retailer ID is numeric[9]");
                        sl.AddDataValidation(dv);

                    }
                    else
                    {

                    }
                    sl.SetRowStyle(1, styleheader);

                    Parallel.For(0, ErrorTable.Rows.Count, new ParallelOptions { MaxDegreeOfParallelism = 1000 }, index =>
                    {
                        sl.SetCellStyle(Convert.ToInt32(ErrorTable.Rows[index]["rownumber"]) + 1, Convert.ToInt32(ErrorTable.Rows[index]["colnumber"]), style4);
                    }); 
                    sl.SaveAs(Path);
                }
            }
           }
}