﻿using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeneriCExcelOperations.CommonClass
{
    /// <summary>
    /// This class is used for storing records which is used globly.
    /// </summary>
    public class clsResultOutput
    {
        /// <summary>
        /// Selected table name
        /// </summary>
        public static string TableName { get; set; }
        /// <summary>
        /// Selected table value.
        /// </summary>
        public static string TableValue { get; set; }
        /// <summary>
        /// Total number of records.
        /// </summary>
        public static string TotalRecords { get; set; }
        /// <summary>
        /// Total Invalid records.
        /// </summary>
        public static string InvalidRecords { get; set; }
        /// <summary>
        /// Total Invalid cells.
        /// </summary>
        public static string InvalidCell { get; set; }
        /// <summary>
        /// Total Valid records.
        /// </summary>
        public static string ValidRecords { get; set; }
        /// <summary>
        /// Source path of file.
        /// </summary>
        public static string SourcePath { get; set; }
        /// <summary>
        /// Server source path.
        /// </summary>
        public static string ServerSourcePath { get; set; }
        /// <summary>
        /// Error container data table.
        /// </summary>
        public static DataTable errorTable { get; set; }
    }
}