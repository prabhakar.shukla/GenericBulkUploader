﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using SpreadsheetLight;
using DocumentFormat.OpenXml.Spreadsheet;
using OfficeOpenXml;
using DocumentFormat.OpenXml.Packaging;
/// <summary>
/// Summary description for clsSpeadSheet
/// </summary>
public class clsSpeadSheet
{
    public clsSpeadSheet()
    {
        //
        // TODO: Add constructor logic here
        //
    }

  
 
    public static MemoryStream TemplateUrl(DataTable templatelist, string fileName)
    {

        DataTable dt = templatelist;
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                             .Select(x => x.ColumnName)
                             .ToArray();
        MemoryStream ms = new MemoryStream();
        using (SLDocument sl = new SLDocument())
        {
            sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, fileName);
            sl.SelectWorksheet(fileName);

       
            for (int h = 0; h <= columnNames.Length - 1; h++)
            {
                sl.SetCellValue(1, h + 1, columnNames[h].ToString());
            }
            // sl.SetRowStyle(1, styleheader);
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {

                for (int j = 0; j <= dt.Columns.Count - 1; j++)
                {
                    var strdatatype = (dt.Rows[i][j].GetType());
                    if (strdatatype.FullName == "System.String")
                    {
                        sl.SetCellValue(i + 2, j + 1, dt.Rows[i][j].ToString());

                    }
                    else if (strdatatype.FullName == "System.Decimal")
                    {
                        sl.SetCellValue(i + 2, j + 1, Convert.ToDecimal(dt.Rows[i][j]));
                    }
                    else if (strdatatype.FullName == "System.ToInt64")
                    {
                        sl.SetCellValue(i + 2, j + 1, Convert.ToInt64(dt.Rows[i][j]));
                    }
                    else
                    {

                        sl.SetCellValue(i + 2, j + 1, Convert.ToString(dt.Rows[i][j]));


                    }

                }
            }
            //sl.FreezePanes(1,6);
            //sl.HideColumn(1, 1);
            sl.SaveAs(ms);
        }

        ms.Position = 0;
        return ms;
    }

    public DataTable GetfileExceldata(string url)
    {
        int iserror = 0;
        DataTable dtsource = new DataTable();
     
            Stream Excelstream = GetStreamFromUrl(url);
            using (ExcelPackage obj = new ExcelPackage(Excelstream))
            {
                dtsource = obj.ToDataTable();


            }
        
       
        return dtsource;
    }

private static Stream GetStreamFromUrl(string url)
    {
        byte[] imageData = null;

        using (var wc = new System.Net.WebClient())
            imageData = wc.DownloadData(url);

        return new MemoryStream(imageData);
    }
   
}


public static class ExcelPackageExtensions
{
    public static DataTable ToDataTable(this ExcelPackage package)
    {
        ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
        DataTable table = new DataTable();
        foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
        {
            table.Columns.Add(firstRowCell.Text);
        }

        for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
        {
            var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
            var newRow = table.NewRow();
            foreach (var cell in row)
            {
                newRow[cell.Start.Column - 1] = cell.Text;
            }
            table.Rows.Add(newRow);
        }
        return table;
    }


    public static void ExportDataSet(DataTable table, string destination)
    {
        using(var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
        {
            var workbookPart = workbook.AddWorkbookPart();

            workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

            //foreach (System.Data.DataTable table in ds.Tables)
            //{

            var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
            var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
            sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

            DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
            string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

            uint sheetId = 1;
            if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
            {
                sheetId =
                    sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
            sheets.Append(sheet);

            DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

            List<String> columns = new List<string>();
            foreach (System.Data.DataColumn column in table.Columns)
            {
                columns.Add(column.ColumnName);

                DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                headerRow.AppendChild(cell);
            }


            sheetData.AppendChild(headerRow);

            foreach (System.Data.DataRow dsrow in table.Rows)
            {
                DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                foreach (String col in columns)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                    newRow.AppendChild(cell);
                }

                sheetData.AppendChild(newRow);
            }

            // }
        }

    }

}