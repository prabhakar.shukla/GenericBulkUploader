


/****** Object:  Table [dbo].[upld_PointExpiry]    Script Date: 08-07-17 11:22:04 ******/
CREATE TABLE [dbo].[upld_PointExpiry](
	[Record_ID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Retailer_ID] [nvarchar](15) NULL,
	[Month] [nvarchar](25) NULL,
	[Points] [numeric](30, 0) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_MyTable_Inserted1]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NULL,
	[Status] [bit] NULL CONSTRAINT [DF__upld_point__Statu__1B89C169]  DEFAULT ((1)),
	[CreatedBy] [nvarchar](50) NULL
) ON [PRIMARY]

GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE PROC [dbo].[USP_SelectBulkUploadTables]

	AS

	SELECT name TableName,object_id UniqueId 
	  FROM sys.tables ST 
	 WHERE SUBSTRING(ST.name,0,5) = 'upld'
  ORDER BY ST.object_id







GO
/****** Object:  StoredProcedure [dbo].[USP_GetExcelFormatOfTable]    Script Date: 08-07-17 11:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROC [dbo].[USP_GetExcelFormatOfTable]
(
@TableName VARCHAR(50),
@TypeOfQuery VARCHAR(50)
)
AS	
DECLARE @SQL VARCHAR(MAX)
DECLARE @QUERY1 VARCHAR(MAX)
DECLARE @cols VARCHAR(max), @query VARCHAR(max)
--SELECT  @cols = STUFF



--    (



--        ( 



--            SELECT '], [' + name



--            FROM sys.columns



--            where object_id = (



--                select top 1 object_id from sys.objects



--                where name = ''+@TableName+''



--            )



--           -- and name not in ('CreatedDate','ModifiedDate','Status')



--            FOR XML PATH('')



--        ), 1, 2, ''



--    ) + ']'


IF(@TypeOfQuery = '1')-- Without Data
BEGIN
SELECT @SQL = 'SELECT * FROM ' + QuoteName(@TableName) + ' WHERE 1 = 0'
END
ELSE IF(@TypeOfQuery = '2') -- With Data
BEGIN
IF(@TableName = 'upld_PointsUploadedDetails')
BEGIN

SET	@QUERY1 = 'WITH TempEmp (SD,Batch,Upload_Upto,Upload_Date,Year,Towards,Detail,Retailer_ID,Points_Uploaded,duplicateRecCount)

AS

(

SELECT SD,Batch,Upload_Upto,Upload_Date,Year,Towards,Detail,Retailer_ID,Points_Uploaded,ROW_NUMBER() OVER(PARTITION by SD,Batch,Upload_Upto,Upload_Date,Year,Towards,Detail,Retailer_ID,Points_Uploaded ORDER BY Record_ID) 

AS duplicateRecCount

FROM ' + QuoteName(@TableName) + '

)



DELETE FROM TempEmp

WHERE duplicateRecCount > 1 '



		SELECT @SQL = 'SELECT * FROM ' + QuoteName(@TableName) + ' ORDER BY Record_ID'

END

ELSE IF (@TableName = 'upld_redemption')

BEGIN

SET	@QUERY1 = 'WITH TempEmp (Retailer_ID,card,ALLOCATED_POINTS,order_at_place,Category,duplicateRecCount)

AS

(

SELECT Retailer_ID,card,ALLOCATED_POINTS,order_at_place,Category,ROW_NUMBER() OVER(PARTITION by Retailer_ID,card,ALLOCATED_POINTS,order_at_place,Category ORDER BY Record_ID) 

AS duplicateRecCount

FROM ' + QuoteName(@TableName) + '

)



DELETE FROM TempEmp

WHERE duplicateRecCount > 1 '



		SELECT @SQL = 'SELECT * FROM ' + QuoteName(@TableName) + ' ORDER BY Record_ID'

END
ELSE IF(@TableName = 'upld_PointExpiry')
BEGIN
SET	@QUERY1 = 'WITH TempEmp (Retailer_ID,Month,Points,duplicateRecCount)

AS

(

SELECT Retailer_ID,Month,Points,ROW_NUMBER() OVER(PARTITION by Retailer_ID,Month,Points ORDER BY Record_ID) 

AS duplicateRecCount

FROM ' + QuoteName(@TableName) + '

)



DELETE FROM TempEmp

WHERE duplicateRecCount > 1 '



		SELECT @SQL = 'SELECT * FROM ' + QuoteName(@TableName) + ' ORDER BY Record_ID'


END
ELSE
BEGIN

SELECT @SQL = 'SELECT * FROM ' + QuoteName(@TableName) + ' ORDER BY Record_ID'

END
END

	exec(@QUERY1)

	exec(@SQL)



	


GO
/****** Object:  StoredProcedure [dbo].[USP_GetTableColumnDetails]    Script Date: 08-07-17 11:15:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_GetTableColumnDetails] --'upld_redemption'
(
	@TableName NVARCHAR(100)
)
AS
	DECLARE @SQL NVARCHAR(1500)
	 SELECT @SQL =
                   'SELECT 
					c.name ''Column_Name'',
					t.Name ''Data_type'',
					c.max_length ''Max_Length'',
					c.is_nullable,
					ISNULL(i.is_primary_key, 0) ''Primary_Key''
				FROM    
					sys.columns c
					INNER JOIN 
						sys.types t ON 
							c.user_type_id = t.user_type_id
					LEFT OUTER JOIN 
						sys.index_columns ic ON 
							ic.object_id = c.object_id AND ic.column_id = c.column_id
					LEFT OUTER JOIN 
						sys.indexes i ON 
							ic.object_id = i.object_id AND ic.index_id = i.index_id
			   WHERE
					c.object_id = OBJECT_ID('''+@TableName+''')'-- and c.name not in (''CreatedDate'',''ModifiedDate'',''Status'') '

	EXEC(@SQL)


GO
/****** Object:  StoredProcedure [dbo].[USP_CreateTableCopy]    Script Date: 08-07-17 11:15:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_CreateTableCopy] --'upld_Retailer'
(
	@TableName NVARCHAR(100)
)

AS
   

     DECLARE @SQL VARCHAR(8000)
	DECLARE @SQLDROP NVARCHAR(1500)
	DECLARE @TableNameTest NVARCHAR(1500)
	DECLARE @cols VARCHAR(max)
	SET @TableNameTest = @TableName+ '_Staging';
	
	-- This line will delete the test table if it exist in the db.
	    SELECT @SQLDROP = 'IF OBJECT_ID('''+@TableNameTest+''', ''U'') IS NOT NULL DROP TABLE '+@TableNameTest+''; 
	      EXEC(@SQLDROP)

 -- This line will create all columns except recordId
		  SELECT  @cols = STUFF   
			(
				( 
					SELECT DISTINCT '], [' + name
					  FROM sys.columns
					 WHERE object_id = (
										SELECT top 1 object_id 
										  FROM sys.objects
										 WHERE name = ''+@TableName+''
										)
					and name not in ('Record_ID')
					FOR XML PATH('')
				), 1, 2, ''
			) + ']'

		SELECT @SQL = 'SELECT CAST(0 as nvarchar(10))[Action] , CAST(0 as nvarchar(20))[Record_ID],'+ @cols + '
						INTO '+@TableNameTest+'
						FROM '+@TableName+'
						WHERE 1 = 0
						UNION ALL
                       select '''','''','+ @cols + ' FROM '+@TableName+' where 1 = 0';
    
	     EXEC(@SQL)



GO
/****** Object:  StoredProcedure [dbo].[USP_CopyDataFromStagingToUpld]    Script Date: 08-07-17 11:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_CopyDataFromStagingToUpld] --'upld_PointExpiry'
(
	@TableName NVARCHAR(100)
)

AS
BEGIN TRY
BEGIN Transaction
if(@TableName='upld_CategoryRedemption')
begin
TRUNCATE TABLE upld_CategoryRedemption
end
	DECLARE @cols VARCHAR(max),@colsUpd VARCHAR(max), @query VARCHAR(max),@queryUpd VARCHAR(max),@queryDel VARCHAR(max), @subQuery VARCHAR(max)
	DECLARE @TableNameTest NVARCHAR(150)
	SET @TableNameTest = @TableName+ '_Staging';
	
		SELECT  @cols = STUFF
			(
				( 
					SELECT DISTINCT '], [' + name
					  FROM sys.columns
					 WHERE object_id = (
										SELECT top 1 object_id 
										  FROM sys.objects
										 WHERE name = ''+@TableNameTest+''
										)
					and name not in ('Action','Record_ID','CreatedDate','ModifiedDate','Status')
					FOR XML PATH('')
				), 1, 2, ''
			) + ']'

		SELECT @query = 'INSERT INTO '+@TableName+'('+@cols+') 
						 SELECT ' + @cols + ' 
						   FROM '+@TableNameTest+'
				  WHERE [Action] = ''A'''
   
  PRINT  @query
   EXEC (@query)                                            -- Copy Data
 
 
 
 SELECT  @colsUpd = STUFF  -- Columns to update data
			(
				( 
					SELECT DISTINCT '], T1.[' + name,']=T2.['+name+''
					  FROM sys.columns
					 WHERE object_id = (
										SELECT top 1 object_id 
										  FROM sys.objects
										 WHERE name = ''+@TableNameTest+''
										)
					and name not in ('Action','Record_ID','CreatedDate','ModifiedDate','Status')
					FOR XML PATH('')
				), 1, 2, ''
			) + ']'

 Select @queryUpd ='Update T1
	SET '+@colsUpd+', T1.[ModifiedDate] = getdate(), T1.[Status] = ''true''
	FROM '+@TableName+' T1
	INNER JOIN '+@TableNameTest+' T2
	ON T1.Record_ID = T2.Record_Id
	WHERE T2.[Action] = ''M'''
	EXEC (@queryUpd)                              -- Update data
 
 
 
 Select @queryDel = 'Update '+@TableName+' set [Status] = ''false'',[ModifiedDate] = getdate()
where Record_ID in( Select Record_ID from '+@TableNameTest+' where [Action] = ''D'')'

EXEC (@queryDel)                              -- Delete data
 
 SELECT @subQuery =  'drop table '+@TableNameTest+''
 EXEC (@subQuery) 
 COMMIT TRANSACTION  
 END TRY
 BEGIN CATCH
 ROLLBACK TRANSACTION
 SELECT ERROR_LINE();
 SELECT ERROR_MESSAGE();
 END CATCH                                        -- Delele table



GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteStagingTable]    Script Date: 08-07-17 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[USP_DeleteStagingTable]

AS

DECLARE @cmd VARCHAR(4000)
DECLARE cmds CURSOR FOR 
	SELECT 'drop table [' + Table_Name + ']'
	FROM INFORMATION_SCHEMA.TABLES
	WHERE Table_Name like '%_Staging'

OPEN cmds
WHILE 1=1
BEGIN
    FETCH cmds INTO @cmd
    IF @@fetch_status != 0 BREAK
    EXEC(@cmd)
END
CLOSE cmds;
DEALLOCATE cmds

