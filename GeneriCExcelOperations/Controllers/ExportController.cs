﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ValidateExcelDataWithAnyTypeValid.Controllers
{
    /// <summary>
    /// This controller is used for Download Excel file.
    /// </summary>
    public class ExportController : Controller
    {
        //
        // GET: /Export/
        /// <summary>
        /// This Action method is used for Downloading a excel File.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
        string filename = Request.QueryString["filename"];
        string FolderName = Request.QueryString["FolderName"];
          downloadanyfile(filename, FolderName);
             return View();
    }
        /// <summary>
        /// This method is a subpart of download file.
        /// </summary>
        /// <param name="filename">File Name</param>
        /// <param name="FolderName">Folder Name where we are saving excel file.</param>
    private void downloadanyfile(string filename, string FolderName)
    {

    
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + "");
        Response.TransmitFile(Server.MapPath("~/" + FolderName + "/" + filename));
        Response.End();
    }
           
        }
	}
