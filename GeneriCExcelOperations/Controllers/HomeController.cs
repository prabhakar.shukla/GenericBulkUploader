﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GeneriCExcelOperations.BAL;
using GeneriCExcelOperations.CommonClass;
using GeneriCExcelOperations.Models;


namespace ValidateExcelDataWithAnyTypeValid.Controllers
{
    /// <summary>
    /// This controller contain all logics for Upload and Download file.
    /// </summary>
    public class HomeController : Controller
    {
        Admin_DLL adminDataAccess = new Admin_DLL();
        int indexCounter = 0;
        DataTable getMisMatchRecords = new DataTable();
        string xlColNoLogger = string.Empty;
        public ActionResult Index()
        {

            return View();
         
        }

      

     
        
        /// <summary>
        /// Using this action method we can upload a selected file from dropdown list.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadFiles()
        {
            DataTable dtExcelData = new DataTable();
            ViewBag.SelectedValue = Request.QueryString["value"];
            ViewBag.SelectedName = ViewBag.SelectedValue.Substring(5);
            string destinationServerFileName = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  
                        HttpPostedFileBase file = files[i];
                        string fname;
                      // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        string sourceFileName = file.FileName;
                        string sourceFileExtension = Path.GetExtension(file.FileName);
                        if (sourceFileExtension != ".xlsx")
                        {
                            return Json("Please select excel file with extension .xlsx");
                        }

                        destinationServerFileName = DateTime.Now.ToString("yyyyMMdd_HHmm") + "_" + ViewBag.SelectedName;
                        string savedFileAtServer = Server.MapPath("~") + "\\Upload" + "\\" + destinationServerFileName + sourceFileExtension;
                        // Get the complete folder path and store the file inside it.  
                      //  fname = Path.Combine(Server.MapPath("~/Upload/"), fname);
                        DirectoryInfo diTemp;
                        diTemp = new DirectoryInfo(Server.MapPath("~/Upload"));
                        if (!diTemp.Exists)
                            diTemp.Create();
                       file.SaveAs(savedFileAtServer);
                       ViewBag.sourceFile = sourceFileName;
                       ViewBag.savedFileAtServer = savedFileAtServer;
                       getXLdata(savedFileAtServer, destinationServerFileName, sourceFileExtension, savedFileAtServer);
                     //  dtExcelData = GeteXLDataInDataTable(file);
                    }
                 
                    // Returns message that successfully uploaded  
                    clsResultOutput.TableValue = ViewBag.SelectedValue;
                    clsResultOutput.TableName = ViewBag.SelectedName;
                    clsResultOutput.ServerSourcePath = ViewBag.savedFileAtServer;
                    clsResultOutput.errorTable = ViewBag.datatable;
                    clsResultOutput.SourcePath = destinationServerFileName;
                    if (ViewBag.Error != "Error")
                    {
                        return Json("File Uploaded Successfully!"+',' + ViewBag.lbl_TotalRecords + ',' + ViewBag.lbl_ValidaRecords + ',' + ViewBag.lbl_InvalidRecords + ',' + ViewBag.lbl_InvalidCells);
                    }
                    else
                    {
                        return Json(ViewBag.lblMessage + ',' + ViewBag.lbl_TotalRecords + ',' + ViewBag.lbl_ValidaRecords + ',' + ViewBag.lbl_InvalidRecords + ',' + ViewBag.lbl_InvalidCells);
                    }
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }

        }

   /// <summary>
   /// Using this method we find excel data and where this xcel file in application directory.
   /// </summary>
   /// <param name="path">where the excel file is saved.</param>
   /// <param name="filedestloc">file destination location </param>
   /// <param name="filesourceloc">file source location</param>
   /// <param name="savedFileAtServer">file path where file is save at application.</param>
        private void getXLdata(string path, string filedestloc, string filesourceloc, string savedFileAtServer)
        {
            try
            {
                bool validHeaderRow = true;
                //start here 
                string columnNames = "";

                DataView dvDatabaseColumns;
                DataTable dtSelectDatabseColumn;
                clsSpeadSheet clsSrd = new clsSpeadSheet();
                DataTable excelDataTable = clsSrd.GetfileExceldata(path);
                DataTable dtDatabaseColumns = adminDataAccess.getTableCloumnDetail(ViewBag.SelectedValue);

                    // Matching ColumnNames with table's name
                    List<string> excelColumns = new List<string>();
                    foreach (DataColumn column in excelDataTable.Columns)
                    {
                        excelColumns.Add(column.ColumnName);
                    }
                    string[] arrExcelColumns = excelColumns.ToArray();
                    columnNames = "";
                    dvDatabaseColumns = new DataView(dtDatabaseColumns);
                    int indexer = 0;
                    foreach (string individualColumnName in arrExcelColumns)
                    {
                        indexer = indexer + 1;
                        if (indexer > 1)
                        {
                            dvDatabaseColumns.RowFilter = "";
                            dvDatabaseColumns.RowFilter = "Column_Name = '" + individualColumnName + "'";

                            dtSelectDatabseColumn = dvDatabaseColumns.ToTable();

                            if (dtSelectDatabseColumn.Rows.Count == 0)
                            {
                                validHeaderRow = false;
                                columnNames += individualColumnName + ", ";
                            }
                        }
                    }
                   //string colNmae = arrExcelColumns[0].ToString();
                    int count = TotalRowsCount(path);
                    string colNmae = GetCellValue(path, ViewBag.SelectedName, "A1", "A");
                    if (colNmae == null)
                    {
                        colNmae = GetCellValue(path, ViewBag.SelectedValue, "A1", "A");
                        if (colNmae == null)
                        {
                            colNmae = GetCellValue(path, "Sheet1", "A1", "A");
                        }
                    }

                    if (colNmae == null)
                    {
                       ViewBag.lblMessage = "Sheet Name miss match . Please Write sheet name according to selected Table";
                       ViewBag.Error = "Error";
                      //  lblMessage.Style.Add("Color", "Red");
                        return;
                    }
                    if (colNmae.Trim().ToLower() != "action")
                    {
                        columnNames += colNmae + ", ";
                    }

                    if (columnNames.Length > 0)
                    {
                        columnNames = columnNames.Substring(0, columnNames.Length - 2);
                    }

                    if (validHeaderRow == false)
                    {
                       ViewBag.lblMessage = @"Excel's columns " + columnNames + " do not match with table's columns.";
                       ViewBag.Error = "Error";
                       return;
                    }
                   TemplateUrl(excelDataTable, dtDatabaseColumns, ViewBag.SelectedName, filedestloc, filesourceloc, savedFileAtServer, count);
                 
                }
            
            catch (Exception ex)
            {
                ViewBag.lblMessage = ex.Message.ToString();
                ViewBag.Error = "Error";
            }
            //end here

        }

        /// <summary>
        /// using this method we can find total number of rows.
        /// </summary>
        /// <param name="path">where excel is saved.</param>
        /// <returns></returns>
        public static int TotalRowsCount(string path)
        {
            int rowsCount = 0;
            using (SpreadsheetDocument myDoc = SpreadsheetDocument.Open(path, false))
            {
                //Get workbookpart
                WorkbookPart workbookPart = myDoc.WorkbookPart;

                //then access to the worksheet part
                IEnumerable<WorksheetPart> worksheetPart = workbookPart.WorksheetParts;

                foreach (WorksheetPart WSP in worksheetPart)
                {
                    //find sheet data
                    IEnumerable<SheetData> sheetData = WSP.Worksheet.Elements<SheetData>();
                    // Iterate through every sheet inside Excel sheet
                    foreach (SheetData SD in sheetData)
                    {
                        IEnumerable<Row> row = SD.Elements<Row>(); // Get the row IEnumerator
                        Console.WriteLine(row.Count()); // Will give you the count of rows
                        rowsCount = row.Count();
                    }

                }

                return rowsCount;

            }
        }
        /// <summary>
        /// Using this method we can find total cell value.
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <param name="sheetName">sheet name</param>
        /// <param name="addressName">location of file</param>
        /// <param name="val">cell value</param>
        /// <returns></returns>
        private static string GetCellValue(string fileName, string sheetName, string addressName, string val)
        {
            string value = null;
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, false))
                {
                    WorkbookPart wbPart = document.WorkbookPart;

                    // Find the sheet with the supplied name, and then use that Sheet
                    // object to retrieve a reference to the appropriate worksheet.
                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                      Where(s => s.Name == sheetName).FirstOrDefault();

                    if (theSheet == null)
                    {
                        throw new ArgumentException("sheetName");
                    }

                    // Retrieve a reference to the worksheet part, and then use its 
                    // Worksheet property to get a reference to the cell whose 
                    // address matches the address you supplied:
                    WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));
                    Cell theCell = wsPart.Worksheet.Descendants<Cell>().
                      Where(c => c.CellReference == addressName).FirstOrDefault();

                    // If the cell does not exist, return an empty string:
                    if (theCell != null)
                    {
                        value = theCell.InnerText;

                        // If the cell represents a numeric value, you are done. 
                        // For dates, this code returns the serialized value that 
                        // represents the date. The code handles strings and Booleans
                        // individually. For shared strings, the code looks up the 
                        // corresponding value in the shared string table. For Booleans, 
                        // the code converts the value into the words TRUE or FALSE.
                        if (theCell.DataType != null && val != "D")
                        {
                            switch (theCell.DataType.Value)
                            {
                                case CellValues.SharedString:
                                    // For shared strings, look up the value in the shared 
                                    // strings table.
                                    var stringTable = wbPart.
                                      GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                                    // If the shared string table is missing, something is 
                                    // wrong. Return the index that you found in the cell.
                                    // Otherwise, look up the correct text in the table.
                                    if (stringTable != null)
                                    {
                                        value = stringTable.SharedStringTable.
                                          ElementAt(int.Parse(value)).InnerText;
                                    }
                                    break;

                                case CellValues.Boolean:
                                    switch (value)
                                    {
                                        case "0":
                                            value = "FALSE";
                                            break;
                                        default:
                                            value = "TRUE";
                                            break;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Write you exceptional logic here
            }
            return value;
        }

        /// <summary>
        /// Using this method we divide a data table into multitable for validating the table records.
        /// </summary>
        /// <param name="excelDataTable">this table contains complete records</param>
        /// <param name="dtDatabaseColumns">this table contains details of DB tables</param>
        /// <param name="fileName">file name</param>
        /// <param name="destinationfilename">Destination location</param>
        /// <param name="sourceFileExtension">Source location</param>
        /// <param name="savedFileAtServer">File location at server</param>
        /// <param name="totalrows">Number of rows</param>
        public void TemplateUrl(DataTable excelDataTable, DataTable dtDatabaseColumns, string fileName, string destinationfilename, string sourceFileExtension, string savedFileAtServer, int totalrows)
        {
            
            DataTable dataTableToProcess = excelDataTable;
            string[] columnNames = dataTableToProcess.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();



            DataTable resultTable = new DataTable();
            DataTable dtError = new DataTable();
            /////more than one column case use below code inside a loop with count of columns
            dtError.Columns.Add("rownumber", typeof(string));
            dtError.Columns.Add("colnumber", typeof(string));
            dtError.Columns.Add("errorcomment", typeof(string));

          
            int index = 0;
            int batchSize = 50;
            try
            {
               
                    List<DataTable> splitdt = SplitTable(dataTableToProcess, 50);

                    List<DataTable> dtErrorContainer = new List<DataTable>();

                // Using Multi Threading Programming which divided data table into 
                //multi part and then validate fiele in same time

                    Task taskObject;
                    var tasks = new ConcurrentBag<Task>();
                    foreach (DataTable item in splitdt)
                    {
                        DataTable dterr = dtError.Clone();
                        taskObject = Task.Factory.StartNew(() => checkdataValidation(item, dtDatabaseColumns, dterr, totalrows, index));
                        dtErrorContainer.Add(dterr);
                        tasks.Add(taskObject);
                        taskObject.Wait();
                        index = index + batchSize;

                    }

                    try
                    {
                        Task.WaitAll(tasks.ToArray());

                        foreach (DataTable dtMerge in dtErrorContainer)
                        {
                            foreach (DataRow drEr in dtMerge.Rows)
                            {
                                dtError.ImportRow(drEr);
                            }
                        }
                        if (dtError.Rows.Count > 13000)
                        {
                           ViewBag.datatable = GetFirstThreeRowViaLinq(dtError);

                            getDataTable.getData = dtError;
                        }
                        else
                        {
                            ViewBag.datatable = dtError;
                            getDataTable.getData = dtError;
                        }
                    }

                    catch (AggregateException e)
                    {
                        Console.WriteLine("\nAggregateException thrown with the following inner exceptions:");
                        // Display information about each exception. 
                        foreach (var v in e.InnerExceptions)
                        {
                            if (v is TaskCanceledException)
                                Console.WriteLine("   TaskCanceledException: Task {0}",
                                                  ((TaskCanceledException)v).Task.Id);
                            else
                                Console.WriteLine("   Exception: {0}", v.GetType().Name);
                        }
                        Console.WriteLine();
                    }
                  
                SendDataToDataBase(dtError, dataTableToProcess, destinationfilename, sourceFileExtension, savedFileAtServer);
            }
            catch (Exception ex)
            {
                //Write you exceptional logic here
            }
 }


        /// <summary>
        /// Using this method we divide our table into mutliple batches
        /// </summary>
        /// <param name="originalTable">Orginal table which is uploaded by user</param>
        /// <param name="batchSize">Lenght of batch size</param>
        /// <returns></returns>
        private static List<DataTable> SplitTable(DataTable originalTable, int batchSize)
        {
            List<DataTable> tables = new List<DataTable>();
            int iSwapeValue = 0;
            int jSwapeValue = 1;
            DataTable newDt = originalTable.Clone();
            newDt.TableName = "Table_" + jSwapeValue;
            newDt.Clear();
            foreach (DataRow row in originalTable.Rows)
            {
                DataRow newRow = newDt.NewRow();
                newRow.ItemArray = row.ItemArray;
                newDt.Rows.Add(newRow);
                iSwapeValue++;
                //For first time 
                if (iSwapeValue == batchSize)
                {
                    tables.Add(newDt);
                    jSwapeValue++;
                    newDt = originalTable.Clone();
                    newDt.TableName = "Table_" + jSwapeValue;
                    newDt.Clear();
                    iSwapeValue = 0;
                }
            }
            //For second time
            if (newDt.Rows.Count > 0 && iSwapeValue != 0)
            {
                tables.Add(newDt);
                jSwapeValue++;
                newDt = originalTable.Clone();
                newDt.TableName = "Table_" + jSwapeValue;
                newDt.Clear();
                iSwapeValue = 0;
            }
            return tables;
        }



        /// <summary>
        /// By using this methode we identityfy rows is valid or not and we insert record in error table if table is not valid.
        /// </summary>
        /// <param name="checkDataRow">Batch size of data table</param>
        /// <param name="dtDatabaseColumns">DB Table details</param>
        /// <param name="dtError">Error table details</param>
        /// <param name="totalrows">number of rows</param>
        /// <param name="rowsindex">statring index of rows</param>
        public void checkdataValidation(DataTable checkDataRow, DataTable dtDatabaseColumns, DataTable dtError, int totalrows, int rowsindex)
        {
            try
            {
                DataTable resultTable = new DataTable();

                for (int rowscount = 0; rowscount <= checkDataRow.Rows.Count - 1; rowscount++)
                {

                    for (int columncount = 0; columncount <= checkDataRow.Columns.Count - 1; columncount++)
                    {
                        var ColumnName = checkDataRow.Columns[columncount].ColumnName.ToString();

                        string expression;
                        DataTable resultDataTable = new DataTable();
                        expression = "Column_Name = '" + ColumnName.Trim() + "'";
                        DataRow[] foundRows;

                        // Use the Select method to find all rows matching the filter.
                        try
                        {
                            foundRows = dtDatabaseColumns.Select(expression);
                        }
                        catch (Exception ex)
                        {
                            foundRows = null;
                            //write your error log for Database.
                        }
                        ////////////add new row after adding columns
                        if (foundRows.Count() > 0)
                        {
                            resultDataTable = foundRows.CopyToDataTable();
                            if (ColumnName == resultDataTable.Rows[0][0].ToString())
                            {
                                if (checkDataRow.Rows[rowscount][columncount - 1].ToString().ToLower() == "a" && ColumnName.ToLower() == "record_id")
                                {
                                    checkDataRow.Rows[rowscount][columncount] = "";
                                }
                                else
                                {
                                    if (!(ColumnName.ToString().Contains("CreatedDate") || ColumnName.ToString().Contains("ModifiedDate") || ColumnName.ToString().Contains("Status")))
                                    {
                                        string result = "";
                                        if (checkDataRow.Rows[rowscount][columncount].ToString() != "")
                                        {
                                            result = ValidateFieldForExcel(resultDataTable, checkDataRow.Rows[rowscount][columncount].ToString(), ColumnName);

                                            if (result != "true")
                                            {
                                                indexCounter = indexCounter + 1;
                                                DataRow dr1 = dtError.NewRow();
                                                dr1["rownumber"] = rowscount + 1 + rowsindex;
                                                dr1["colnumber"] = columncount + 1;
                                                dr1["errorcomment"] = result;
                                                dtError.Rows.Add(dr1.ItemArray);
                                            }

                                            ViewBag.datatable = dtError;
                                            ViewBag.lbl_TotalRecords = (totalrows - 1).ToString();
                                            resultTable = dtError;
                                            ViewBag.lbl_InvalidRecords = resultTable.DefaultView.ToTable(true, "rownumber").Rows.Count.ToString();
                                            ViewBag.lbl_InvalidCells = indexCounter.ToString();
                                            ViewBag.lbl_ValidaRecords = (totalrows - 1 - resultTable.DefaultView.ToTable(true, "rownumber").Rows.Count).ToString();
                                           }
                                        else
                                        {
                                            result = "Please enter the values in others columns except the 'Record ID','Created date',' Modified Date' and 'Status'.";
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //write you log file here
            }

        }


        /// <summary>
        /// Using this method we send data in Database
        /// </summary>
        /// <param name="dtError"> this table contains complete error if error occured</param>
        /// <param name="dtSendDataToDataBaes">this table data we send in database</param>
        /// <param name="destinationfilename">this string contains file addres where file is saved</param>
        /// <param name="sourceFileExtension">this string contains extension of file</param>
        /// <param name="savedFileAtServer">this string contains path where file is saved at server</param>
        public void SendDataToDataBase(DataTable dtError, DataTable dtSendDataToDataBaes, string destinationfilename, string sourceFileExtension, string savedFileAtServer)
        {
            Int32 getRowsCount = dtSendDataToDataBaes.Rows.Count;
            if (dtError.Rows.Count == 0)
            {
                try
                {
                    if (ViewBag.SelectedName == "CategoryRedemption")
                    {
                        dtSendDataToDataBaes = (DataTable)ViewBag.validContentIds;
                    }
                    string destinationTableName = ViewBag.SelectedValue + "_Staging";
                    DataTable result = dtSendDataToDataBaes.Clone();
                    foreach (DataRow dr in dtSendDataToDataBaes.Rows)
                    {
                        if (dr["Action"].ToString().ToLower() == "a" || dr["Action"].ToString().ToLower() == "m" || dr["Action"].ToString().ToLower() == "d")
                            result.Rows.Add(dr.ItemArray);
                    }
                    getRowsCount = result.Rows.Count;
                    //for removing space
                    for (int i = 0; i < result.Rows.Count; i++)
                        for (int j = 0; j < result.Columns.Count; j++)
                        {
                            string o = Convert.ToString(result.Rows[i][j]);
                            if (o.Length > 0)
                            {
                                if (o.Trim().Length == 0)
                                {
                                    result.Rows[i][j] = null;
                                    if (result.Columns[j].ColumnName == "Retailer_ID" || result.Columns[j].ColumnName == "card" || result.Columns[j].ColumnName == "ALLOCATED_POINTS" || result.Columns[j].ColumnName == "Year" || result.Columns[j].ColumnName == "Points_Uploaded")
                                    {
                                        result.Rows[i][j] = 0;
                                    }
                                    result.AcceptChanges();
                                }
                            }
                            else
                            {
                                if (result.Columns[j].ColumnName == "Retailer_ID" || result.Columns[j].ColumnName == "card" || result.Columns[j].ColumnName == "ALLOCATED_POINTS" || result.Columns[j].ColumnName == "Year" || result.Columns[j].ColumnName == "Points_Uploaded")
                                {
                                    result.Rows[i][j] = 0;
                                }
                                result.AcceptChanges();
                            }
                         
                        }

                    //   end here
                    if (result.Rows.Count > 0)
                    {
                        xlColNoLogger = "creating table copy starts.";

                        // creates a staging table
                        adminDataAccess.createTableCopy(ViewBag.SelectedValue); 
                        xlColNoLogger = "bulk upload for table starts.";
                   
                        if (result.Columns.Contains("Status"))
                        {
                            result.Columns.Remove("Status");
                            result.AcceptChanges();
                        }
                        if (result.Columns.Contains("CreatedDate"))
                        {
                            result.Columns.Remove("CreatedDate");
                            result.AcceptChanges();
                        }
                        if (result.Columns.Contains("ModifiedDate"))
                        {
                            result.Columns.Remove("ModifiedDate");
                            result.AcceptChanges();
                        }
                      
                        // This method is used for  storing data.

                        adminDataAccess.BulkUpload(result, destinationTableName); // bulk uploading of data
                        xlColNoLogger = "copy data from staging starts.";

                        //This method is used for copy data from staging t upload table.
                        adminDataAccess.CopyDataFromStagingToUpld(ViewBag.SelectedValue); // shifting data from staging to orignal and deleting staging table.


                        ViewBag.lbl_TotalRecords = (result.Rows.Count).ToString();

                        if (ViewBag.datatable != null)
                        {
                            ViewBag.lbl_InvalidRecords = getDataTable.getData.DefaultView.ToTable(true, "rownumber").Rows.Count.ToString();
                            ViewBag.lbl_ValidaRecords = (result.Rows.Count - getDataTable.getData.DefaultView.ToTable(true, "rownumber").Rows.Count).ToString();
                        }
                        else
                        {
                            ViewBag.lbl_InvalidRecords = "0";
                            ViewBag.lbl_ValidaRecords = (result.Rows.Count - 0).ToString();
                            ViewBag.lbl_InvalidCells = "0";
                            ViewBag.lblMessage = "Uploaded successfully!";
                        
                        }
                    
                    }
                    ViewBag.lbl_TotalRecords = (result.Rows.Count + Convert.ToInt16(ViewBag.lbl_MishMatch)).ToString();
                    ViewBag.lbl_InvalidRecords = "0";
                    ViewBag.lbl_ValidaRecords = (result.Rows.Count - 0).ToString();
                    ViewBag.lbl_InvalidCells = "0";
                    ViewBag.lblMessage = "Uploaded successfully!";
                    if ( ViewBag.FileLocation != null)
                    {
                        xlColNoLogger = "excel being moved to archieve";
                       
                        DirectoryInfo diTempArchive = new DirectoryInfo(Server.MapPath("~/BulkUpload/Archive"));
                        if (!diTempArchive.Exists)
                            diTempArchive.Create();
                        string fileLocation = Convert.ToString( ViewBag.FileLocation);
                        string sourcePath = Path.GetDirectoryName(fileLocation);
                        string targetPath = Server.MapPath("~") + "BulkUpload\\Archive";
                        // Use Path class to manipulate file and directory paths.
                        string sourceFile = Path.Combine(sourcePath, destinationfilename + sourceFileExtension);
                        string destFile = Path.Combine(targetPath, destinationfilename + sourceFileExtension);

                        // To copy a folder's contents to a new location:
                        // Create a new target folder, if necessary.
                        if (!System.IO.Directory.Exists(targetPath))
                        {
                            System.IO.Directory.CreateDirectory(targetPath);
                        }

                        // To copy a file to another location and 
                        // overwrite the destination file if it already exists.
                        System.IO.File.Copy(sourceFile, destFile, true);

                    }
                }
                catch (Exception ex)
                {
                    // this method is used for removing staging table when we copy the data in orginal table.
                    adminDataAccess.DeleteStagingTable();
                    ViewBag.lblMessage = ex.Message;
                    ViewBag.lbl_TotalRecords = "";
                    ViewBag.lbl_InvalidRecords = "";
                    ViewBag.lbl_ValidaRecords = "";
                    ViewBag.lbl_InvalidCells = "";

                }

            }
            else
            {
             if (ViewBag.datatable != null)
                {
                    ViewBag.Error = "Error";
                    ViewBag.lblMessage = "Excel validation failed!";
                    ViewBag.lbl_TotalRecords = getRowsCount.ToString();
                    ViewBag.lbl_InvalidRecords = getDataTable.getData.DefaultView.ToTable(true, "rownumber").Rows.Count.ToString();
                    ViewBag.lbl_ValidaRecords = (getRowsCount - getDataTable.getData.DefaultView.ToTable(true, "rownumber").Rows.Count).ToString();
                }

            }
        }
        /// <summary>
        /// we have develope the logic for validating column type ,lenghth and custome validation.
        /// </summary>
        /// <param name="dtDatabaseColumns"> for this table we find out exact value of table </param>
        /// <param name="excelColumnValue"> for this  we compare the value of column</param>
        /// <param name="dcExcelColumn">we get the column name</param>
       /// <returns></returns>
        protected string ValidateFieldForExcel(DataTable dtDatabaseColumns, string excelColumnValue, string dcExcelColumn)
        {


            string databaseColumnName = dtDatabaseColumns.Rows[0]["Column_Name"].ToString();
            string databaseColumnType = dtDatabaseColumns.Rows[0]["Data_type"].ToString();
            int databaseColumnLength = Convert.ToInt32(dtDatabaseColumns.Rows[0][2].ToString());

            string error = String.Empty;
            try
            {
                if (dcExcelColumn.Trim().ToUpper() == databaseColumnName.Trim().ToUpper())
                {
                    if (databaseColumnType == "nvarchar" && excelColumnValue != "")  // length check
                    {
                        if (databaseColumnName.ToLower() != "retailer_id")
                        {
                            if (excelColumnValue.Length > databaseColumnLength / 2)
                            {
                                error += databaseColumnName + " length must not be greater than " + databaseColumnLength / 2 + " characters.";
                            }
                            if (databaseColumnName.ToLower() == "upload_upto" || databaseColumnName.ToLower() == "upload_date" || databaseColumnName.ToLower() == "month" || databaseColumnName.ToUpper() == "ACTIVITY_ORDER_DATE")
                            {
                                CultureInfo enUS = new CultureInfo("en-US");
                                //  DateTime val;

                                if (!((excelColumnValue.Replace("/", "-").Replace(",", "-")).Contains("-")))
                                {
                                    error += databaseColumnName + "should be in proper date time format(MM/dd/yyyy).";

                                }
                                if (!excelColumnValue.Any(Char.IsDigit))
                                {
                                    error += databaseColumnName + "should be in proper date time format(MM/dd/yyyy).";
                                }
                              
                            }
                        }
                        else
                        {
                            if (excelColumnValue.Length > databaseColumnLength / 2)
                            {
                                error += databaseColumnName + " length must not be greater than " + databaseColumnLength + " characters.";
                            }
                            int val;
                            bool result = Int32.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }
                        }
                    }
                    if (databaseColumnType == "float" && excelColumnValue != "")  // length check
                    {

                        double val;
                        bool result = Double.TryParse(excelColumnValue, out val);
                        if (!result)
                        {
                            error += databaseColumnName + " must be a decimal value of max length" + databaseColumnLength;
                        }

                    }

                    if (databaseColumnType == "double" && excelColumnValue != "")
                    {

                        double val;
                        bool result = Double.TryParse(excelColumnValue, out val);
                        if (!result)
                        {
                            error += databaseColumnName + " must be a decimal value of max length" + databaseColumnLength;
                        }

                    }
                    else if (databaseColumnType == "int" && excelColumnValue != "")
                    {


                        int val;
                        bool result = Int32.TryParse(excelColumnValue, out val);
                        if (!result)
                        {
                            error += databaseColumnName + " must be a numeric value.";
                        }

                        if (excelColumnValue.ToString().Length > 10 && databaseColumnName.ToLower() != "year")
                        {
                            error += databaseColumnName + " length must not be greater than " + 10;
                        }
                        else if (databaseColumnName.ToLower() == "year" && excelColumnValue.ToString().Length != 4)
                        {
                            error += databaseColumnName + " length must not be greater than " + 4;
                        }

                    }

                    else if (databaseColumnType == "numeric" && excelColumnValue != "")
                    {

                        if (databaseColumnName.ToLower() != "card" && databaseColumnName.ToLower() != "allocated_points" && databaseColumnName.ToLower() != "points_uploaded")
                        {
                            int val;
                            bool result = Int32.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }

                            if (excelColumnValue.ToString().Length > databaseColumnLength)
                            {
                                error += databaseColumnName + " length must not be greater than " + databaseColumnLength;
                            }
                        }
                        if (databaseColumnName.ToLower() == "card")
                        {
                            long val;
                            bool result = Int64.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }

                            if (excelColumnValue.ToString().Length > 16)
                            {
                                error += databaseColumnName + " length must not be greater than " + (16);
                            }
                        }

                        if (databaseColumnName.ToLower() == "points_uploaded")
                        {
                            long val;
                            bool result = Int64.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }

                            if (excelColumnValue.ToString().Length > 17)
                            {
                                error += databaseColumnName + " length must not be greater than " + (16);
                            }
                        }




                        if (databaseColumnName.ToLower() == "allocated_points" || databaseColumnName.ToLower() == "Points")
                        {
                            long val;
                            bool result = Int64.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }

                            if (excelColumnValue.ToString().Length > 15)
                            {
                                error += databaseColumnName + " length must not be greater than " + (15);
                            }
                        }

                    }



                    else if (databaseColumnType == "bigint" && excelColumnValue.Trim() != "")
                    {


                        Int64 val;
                        bool result = Int64.TryParse(excelColumnValue, out val);
                        if (!result)
                        {
                            error += databaseColumnName + " must be a numeric value.";
                        }

                        if (excelColumnValue.ToString().Length > 19)
                        {
                            error += databaseColumnName + " length must not be greater than " + 19;
                        }
                    }

                    else if (databaseColumnType == "datetime" && excelColumnValue.Trim() != "")
                    {

                        CultureInfo enUS = new CultureInfo("en-US");
                        DateTime val;
                        bool result = DateTime.TryParseExact(excelColumnValue, "MM-dd-yy", enUS, DateTimeStyles.None, out val);
                        if (!result)
                        {
                            error += databaseColumnName + "should be in proper date time format(MM-dd-yy).";
                        }

                    }

                    else if (databaseColumnType == "bit" && excelColumnValue.Trim() != "")
                    {
                        string excelColVal = Convert.ToString(excelColumnValue).Trim().ToLower();
                        if (!(excelColVal == "1" || excelColVal == "0"))
                        {
                            error += databaseColumnName + " should be either 1 or 0.";
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                error += databaseColumnName + ex.Message.ToString();
            }

            if (error == "")
                error = "true";
            return error;

        }
        private static DataTable GetFirstThreeRowViaLinq(DataTable dataTable)
        {
            return dataTable.Rows.Cast<System.Data.DataRow>().Take(13000).CopyToDataTable();
        }

        /// <summary>
        /// Getting table name from Database.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList()
        {
            List<clsBindDropDownListPara> obj = new clsBindDropDownList().BindDatabaseTables();
         
             return Json(obj, JsonRequestBehavior.AllowGet);
         
        }
        /// <summary>
        /// This method is used for downloading an excel file
        /// </summary>
        /// <param name="exportHeader">File Name</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DownLoadXLFile(string exportHeader)
        {
            string tableName = Request.QueryString["tableName"];
            string typeData = Request.QueryString["typeData"];
            string path =  DownLoadXLFile(tableName, typeData, exportHeader);
            return Json(path, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This method is used for download table from Database.
        /// </summary>
        /// <param name="tableName">Selected table from dropdownlist.</param>
        /// <param name="typeData"> This string used for table will with data or without data this contains 1and 2 values.</param>
        /// <param name="exportHeader">This string is the name of downloaded excel file.</param>
        /// <returns></returns>
        protected string DownLoadXLFile(string tableName, string typeData, string exportHeader)
        {
            string FilePath = "";
            ViewBag.SelectedValue = tableName;
            ViewBag.SelectedName = tableName.Substring(5);
            string i = typeData;
            string saveLocation = "";
            try
            {
                if (tableName.Length > 0)
                {
                    DataTable dt = adminDataAccess.getExcelFormetOfSelectedTable(ViewBag.SelectedValue,i);
                    int count = dt.Columns.Count;
                    dt.Columns.Add("Action").SetOrdinal(0);

                   // string rowValue = "";
                    if (ViewBag.SelectedValue == "upld_PointsUploadedDetails")
                    {
                        RemoveNullColumnFromDataTable(dt, "upld_PointsUploadedDetails");

                        // RemoveDuplicates(dt, "upld_PointsUploadedDetails");
                    }
                    else if (ViewBag.SelectedValue == "upld_redemption")
                    {
                        RemoveNullColumnFromDataTable(dt, "upld_redemption");
                        //  RemoveDuplicates(dt, "othertable");
                    }
                    else if (ViewBag.SelectedValue == "upld_PointExpiry")
                    {
                        RemoveNullColumnFromDataTable(dt, "othertable");
                    }
                    else
                    {
                        //for default selection
                    }
                 
                    string path = Server.MapPath("~/Sheets/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    string[] files = Directory.GetFiles(path);

                    foreach (string file in files)
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.LastAccessTime < DateTime.Now.AddMinutes(-1))
                            fi.Delete();
                    }
                    DataSet dsCopy = new DataSet();
                    dsCopy.Tables.Add(dt);
                    dsCopy.Tables[0].TableName = ViewBag.SelectedValue;
                    dsCopy.AcceptChanges();
                    //Using this method we create excel file using Spreadsheet Light.
                    SpreadsheetLightCreatExcelFile.CreateExcelDocument(dsCopy, path+ViewBag.SelectedValue + ".xlsx", "Header", null);
                  
                    FilePath = ViewBag.SelectedValue + ".xlsx";
                 

                }
            }
            catch (Exception ex)
            {
               ViewBag.Error = "Error";
               ViewBag.lblMessage = ex.Message + saveLocation;
             }
            return FilePath;

        }

        /// <summary>
        /// Using this method we remove the nullable rows or column.
        /// </summary>
        /// <param name="DownloadedTable">which table we download from database.</param>
        /// <param name="strTableName">Table Name</param>
        public static void RemoveNullColumnFromDataTable(DataTable DownloadedTable, string strTableName)
        {
            for (int i = DownloadedTable.Rows.Count - 1; i >= 0; i--)
            {
                if (strTableName == "upld_PointsUploadedDetails")
                {
                    if (DownloadedTable.Rows[i]["SD"] == DBNull.Value && DownloadedTable.Rows[i]["Batch"] == DBNull.Value && DownloadedTable.Rows[i]["Upload_Upto"] == DBNull.Value && DownloadedTable.Rows[i]["Upload_Date"] == DBNull.Value && DownloadedTable.Rows[i]["Year"] == DBNull.Value && DownloadedTable.Rows[i]["Towards"] == DBNull.Value && DownloadedTable.Rows[i]["Detail"] == DBNull.Value && DownloadedTable.Rows[i]["Retailer_ID"] == DBNull.Value && DownloadedTable.Rows[i]["Points_Uploaded"] == DBNull.Value)
                    {

                        DownloadedTable.Rows[i].Delete();
                    }
                    if (DownloadedTable.Rows[i]["SD"] == DBNull.Value && DownloadedTable.Rows[i]["Batch"] == DBNull.Value && DownloadedTable.Rows[i]["Upload_Upto"] == DBNull.Value && DownloadedTable.Rows[i]["Upload_Date"] == DBNull.Value && Convert.ToString(DownloadedTable.Rows[i]["Year"]) == "0" && DownloadedTable.Rows[i]["Towards"] == DBNull.Value && DownloadedTable.Rows[i]["Detail"] == DBNull.Value && Convert.ToString(DownloadedTable.Rows[i]["Retailer_ID"]) == "0" && Convert.ToString(DownloadedTable.Rows[i]["Points_Uploaded"]) == "0")
                    {

                        DownloadedTable.Rows[i].Delete();
                    }
                }
                else if (strTableName == "upld_redemption")
                {
                    if (Convert.ToString(DownloadedTable.Rows[i]["card"]) == "0" && Convert.ToString(DownloadedTable.Rows[i]["ALLOCATED_POINTS"]) == "0" && DownloadedTable.Rows[i]["order_at_place"] == DBNull.Value && DownloadedTable.Rows[i]["Category"] == DBNull.Value && DownloadedTable.Rows[i]["Retailer_ID"] == DBNull.Value)
                    {
                        DownloadedTable.Rows[i].Delete();

                    }
                    if (DownloadedTable.Rows[i]["card"] == DBNull.Value && DownloadedTable.Rows[i]["ALLOCATED_POINTS"] == DBNull.Value && DownloadedTable.Rows[i]["order_at_place"] == DBNull.Value && DownloadedTable.Rows[i]["Category"] == DBNull.Value && Convert.ToString(DownloadedTable.Rows[i]["Retailer_ID"]) == "0")
                    {
                        DownloadedTable.Rows[i].Delete();

                    }
                }
                else
                {
                    if (DownloadedTable.Rows[i]["Points"] == DBNull.Value && DownloadedTable.Rows[i]["month"] == DBNull.Value && DownloadedTable.Rows[i]["Retailer_ID"] == DBNull.Value)
                    {
                        DownloadedTable.Rows[i].Delete();

                    }
                    if (DownloadedTable.Rows[i]["Points"] == DBNull.Value && DownloadedTable.Rows[i]["month"] == DBNull.Value && DownloadedTable.Rows[i]["Retailer_ID"] == DBNull.Value)
                    {
                        DownloadedTable.Rows[i].Delete();

                    }
                }
            }
            DownloadedTable.AcceptChanges();
        }

        /// <summary>
        /// This Action method occured when our system got error in excell.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GenrateError()
        {
            string path = GenerateError();
            return Json(path,JsonRequestBehavior.AllowGet);
        }


       /// <summary>
       /// This method is a subpart of Genrate Error Action for genrating error.
       /// </summary>
       /// <returns></returns>
        protected string  GenerateError()
        {
            string path = "";
            try
            {
                if (clsResultOutput.ServerSourcePath != null && clsResultOutput.errorTable != null)
                {
                    clsSetCSS objclsSetCSS = new clsSetCSS();
                    objclsSetCSS.SetErrorStyleSheet(clsResultOutput.ServerSourcePath, clsResultOutput.TableName, getDataTable.getData, clsResultOutput.TableValue);
                    path = clsResultOutput.SourcePath + ".xlsx";
                   
                }
            }
            catch (Exception ex)
            {
              //Write your error log logic here for storing error in DB.
            }
            return path;
        }

    }
}