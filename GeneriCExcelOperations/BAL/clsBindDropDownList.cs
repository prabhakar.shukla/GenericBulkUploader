﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using GeneriCExcelOperations.Models;

namespace GeneriCExcelOperations.BAL
{
    /// <summary>
    /// This class is used for Bind table in dropdownlist with specific name.
    /// </summary>
    public class clsBindDropDownList
    {
        Admin_DLL dl = new Admin_DLL();
        /// <summary>
        /// This method is used for Bind Table in dropdownlist.
        /// </summary>
        /// <returns></returns>
        public List<clsBindDropDownListPara> BindDatabaseTables()
        {
            List<clsBindDropDownListPara> list = new List<clsBindDropDownListPara>();
            try
            {
                DataTable dt = dl.getAllBulkUploadTables();
                dt.Columns.Add("RenameTable").SetOrdinal(0);



                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["TableName"].ToString().Trim() == "upld_PointsUploadedDetails")
                    {
                        dr["RenameTable"] = "PointsUploadedDetails";
                        dt.AcceptChanges();
                    }
                    else if (dr["TableName"].ToString().Trim() == "upld_redemption")
                    {
                        dr["RenameTable"] = "Redemption";
                        dt.AcceptChanges();
                    }
                    else if (dr["TableName"].ToString().Trim() == "upld_PointExpiry")
                    {
                        dr["RenameTable"] = "PointExpiry";
                        dt.AcceptChanges();
                    }
                    else if (dr["TableName"].ToString().Trim() == "upld_PayBackOrderDetails")
                    {
                        dr["RenameTable"] = "PayBackOrderDetails";
                        dt.AcceptChanges();
                    }
                    else if (dr["TableName"].ToString().Trim() == "upld_CardDispatchDetail")
                    {
                        dr["RenameTable"] = "CardDispatchDetail";
                        dt.AcceptChanges();
                    }
                    else
                    {
                        string tableName = dr["TableName"].ToString().Trim().Substring(5, dr["TableName"].ToString().Trim().Length - 5);
                        dr["RenameTable"] = tableName;
                        dt.AcceptChanges();
                    }
                }

              
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    clsBindDropDownListPara BindDropDownList = new clsBindDropDownListPara();
                    BindDropDownList.TableName = dt.Rows[index][0].ToString();
                    BindDropDownList.TableValue = dt.Rows[index][1].ToString();
                    list.Add(BindDropDownList);
                }
            }

            catch (Exception ex)
            {
               //Write you error log file logic here.
            }
            return list;
        }
    }
}