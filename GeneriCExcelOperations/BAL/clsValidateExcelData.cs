﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Xml;
using System.Collections.Concurrent;
using System.Diagnostics;
using _DLL;
using SpreadsheetLight;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;


namespace ValidateExcelDataWithAnyTypeValid.BAL
{
    public class clsValidateExcelData
    {
        Admin_DLL dl = new Admin_DLL();
        readXLdata XL = new readXLdata();

        string xlRowNoLogger = "";
        string xlColNoLogger = "";
        int ik = 0;
        DataTable getMisMatchRecords = new DataTable();
        //Genrate Error
        protected void GenerateError(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["savedFileAtServer"] != null && ViewState["datatable"] != null)
                {
                    SetErrorStyleSheet(ViewState["savedFileAtServer"].ToString(), ddltables.SelectedItem.Text, getDataTable.getData);

                    byte[] byteArray = File.ReadAllBytes(ViewState["savedFileAtServer"].ToString());

                    // Do work here
                    Response.AppendHeader("content-disposition", "attachment; filename=" + Convert.ToString(ViewState["sourceFile"]));
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BinaryWrite(byteArray);
                    Response.End();

                }
            }
            catch (Exception ex)
            {

            }
        }
        //DownLoad Excel Data
        protected void btndownload_OnClick(object sender, EventArgs e)
        {
            string saveLocation = "";
            try
            {
                if (ddltables.SelectedIndex > 0)
                {
                    DataTable dt = dl.getExcelFormetOfSelectedTable(ddltables.SelectedValue, rdoTypeOfQuery.SelectedValue);
                    int count = dt.Columns.Count;
                    dt.Columns.Add("Action").SetOrdinal(0);

                    string rowValue = "";
                    if (ddltables.SelectedValue == "upld_PointsUploadedDetails")
                    {
                        RemoveNullColumnFromDataTable(dt, "upld_PointsUploadedDetails");

                        // RemoveDuplicates(dt, "upld_PointsUploadedDetails");
                    }
                    else if (ddltables.SelectedValue == "upld_redemption")
                    {
                        RemoveNullColumnFromDataTable(dt, "upld_redemption");
                        //  RemoveDuplicates(dt, "othertable");
                    }
                    else if (ddltables.SelectedValue == "upld_PointExpiry")
                    {
                        RemoveNullColumnFromDataTable(dt, "othertable");
                    }
                    else
                    {

                    }
                    foreach (DataRow dr in dt.Rows)
                    {
                        rowValue = Convert.ToString(dr["Record_ID"]);
                        if (dr["Record_ID"] != DBNull.Value)
                        {
                            if (dr["CreatedDate"] != DBNull.Value)
                            {
                                dr["Record_ID"] = Convert.ToDateTime(dr["CreatedDate"]).ToString("yyyyMMdd") + "0000" + rowValue;
                            }
                            else if (dr["ModifiedDate"] != DBNull.Value)
                            {
                                dr["Record_ID"] = Convert.ToDateTime(dr["ModifiedDate"]).ToString("yyyyMMdd") + "0000" + rowValue;
                            }
                        }
                    }




                    byte[] ms = clsSpeadSheet.TemplateUrl(dt, ddltables.SelectedItem.Text).ToArray();

                    Response.AppendHeader("content-disposition", "attachment; filename=" + ddltables.SelectedItem.Text + ".xlsx");
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BinaryWrite(ms);
                    Response.End();


                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + saveLocation;
                lblMessage.Style.Add("Color", "Red");

            }
        }

        // upload excel data
        protected void UploadExcelData(object sender, EventArgs e)
        {
            try
            {

                btnReport.Visible = true;
                btnUnMatchRecord.Visible = false;
                string sourceFileName = Path.GetFileName(flUpload.PostedFile.FileName);
                string sourceFileExtension = Path.GetExtension(flUpload.PostedFile.FileName);

                string destinationServerFileName = DateTime.Now.ToString("yyyyMMdd_HHmm") + "_" + ddltables.SelectedItem.Text;
                string savedFileAtServer = Server.MapPath("~") + "\\BulkUpload\\InProgress" + "\\" + destinationServerFileName + sourceFileExtension;

                //Copy source file to Server Location
                DirectoryInfo diTemp;

                ViewState["sourceFile"] = sourceFileName;
                ViewState["savedFileAtServer"] = savedFileAtServer;

                diTemp = new DirectoryInfo(Server.MapPath("~/BulkUpload/InProgress"));
                if (!diTemp.Exists)
                    diTemp.Create();

                flUpload.SaveAs(savedFileAtServer);

                getXLdata(savedFileAtServer, destinationServerFileName, sourceFileExtension, savedFileAtServer);
                if (ddltables.SelectedItem.Text == "CategoryRedemption")
                {
                    if (!btnReport.Enabled && ViewState["getMisMatchRecords"] != null && lbl_MishMatch.Text != "0")
                    {
                        btnReport.Visible = false;
                        btnUnMatchRecord.Visible = true;

                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
            //if (Session["User_Type"].ToString() == "SD")
            //{
            //    int Count = Convert.ToInt16(dl.DataExecuteScaler("USP_UploadretailerGetCount"));
            //    lblerror.Text = Count.ToString();
            //    pnlerror.Visible = true;
            //}
            //else
            //{
                pnlerror.Visible = false;
          //  }
        }
        private void getXLdata(string path, string filedestloc, string filesourceloc, string savedFileAtServer)
        {
            try
            {
                bool validHeaderRow = true;
                //start here 
                string columnNames = "";

                DataView dvDatabaseColumns;
                DataTable dtSelectDatabseColumn;



                //end here 

                if (flUpload.HasFile)
                {

                    //start here
                    lbl_TotalRecords.Text = "";
                    lbl_InvalidRecords.Text = "";
                    lbl_InvalidCells.Text = "";
                    lbl_ValidaRecords.Text = "";

                    // btnReport.Enabled = true;
                    ddltables.Enabled = false;
                    //end here


                    clsSpeadSheet clsSrd = new clsSpeadSheet();



                    DataTable dt = clsSrd.GetfileExceldata(path);


                    //start here
                    DataTable dtDatabaseColumns = dl.getTableCloumnDetail(ddltables.SelectedValue);

                    // Matching ColumnNames with table's name
                    List<string> excelColumns = new List<string>();



                    foreach (DataColumn column in dt.Columns)
                    {
                        excelColumns.Add(column.ColumnName);
                    }


                    string[] arrExcelColumns = excelColumns.ToArray();
                    columnNames = "";
                    dvDatabaseColumns = new DataView(dtDatabaseColumns);

                    int i = 0;
                    foreach (string individualColumnName in arrExcelColumns)
                    {

                        i = i + 1;
                        if (i > 1)
                        {
                            dvDatabaseColumns.RowFilter = "";
                            dvDatabaseColumns.RowFilter = "Column_Name = '" + individualColumnName + "'";

                            dtSelectDatabseColumn = dvDatabaseColumns.ToTable();

                            if (dtSelectDatabseColumn.Rows.Count == 0)
                            {
                                validHeaderRow = false;
                                columnNames += individualColumnName + ", ";
                            }
                        }
                    }

                    //string colNmae = arrExcelColumns[0].ToString();
                    int count = totalRows1(path);
                    string colNmae = GetCellValue(path, ddltables.SelectedItem.Text, "A1", "A");
                    if (colNmae == null)
                    {
                        colNmae = GetCellValue(path, ddltables.SelectedValue, "A1", "A");
                        if (colNmae == null)
                        {
                            colNmae = GetCellValue(path, "Sheet1", "A1", "A");
                        }
                    }

                    if (colNmae == null)
                    {
                        lblMessage.Text = "Sheet Name miss match . Please Write sheet name according to selected Table";
                        lblMessage.Style.Add("Color", "Red");
                        return;
                    }
                    if (colNmae.Trim().ToLower() != "action")
                    {
                        columnNames += colNmae + ", ";
                    }

                    if (columnNames.Length > 0)
                    {
                        columnNames = columnNames.Substring(0, columnNames.Length - 2);
                    }

                    if (validHeaderRow == false)
                    {
                        lblMessage.Text = @"Excel's columns " + columnNames + " do not match with table's columns.";
                        lblMessage.Style.Add("Color", "Red");
                        return;
                    }

                    // this code only for RedemptionCategory 
                    if (ddltables.SelectedItem.Text == "CategoryRedemption")
                    {
                        DataTable getCateAndSubCategary = dl.getAllCategaryAndSubCategary();
                        var getMatchCatAndSubCag = from table1 in dt.AsEnumerable()
                                                   join table2 in getCateAndSubCategary.AsEnumerable()
                                                   on table1.Field<string>("Category").ToUpper() equals table2.Field<string>("Category").ToUpper()
                                                   where table1.Field<string>("Sub Category").ToUpper() == table2.Field<string>("Sub Category").ToUpper()
                                                   select table1;
                        DataTable validContentIds = new DataTable();
                        if (getMatchCatAndSubCag.ToList().Count > 0)
                        {
                            validContentIds = getMatchCatAndSubCag.CopyToDataTable();
                        }
                        ViewState["validContentIds"] = validContentIds;
                        var idsNotInB = dt.AsEnumerable().Select(r => r.Field<string>("Category").ToUpper())
                                       .Except(validContentIds.AsEnumerable().Select(r => r.Field<string>("Category").ToUpper())).ToList();
                        if (idsNotInB.Count > 0)
                        {
                            DataTable TableB = (from row in dt.AsEnumerable()
                                                join id in idsNotInB
                                                on row.Field<string>("Category").ToUpper() equals id.ToUpper()
                                                select row).CopyToDataTable();
                            getMisMatchRecords.Merge(TableB);
                        }
                        var idsNotInA = dt.AsEnumerable().Select(r => r.Field<string>("Sub Category").ToUpper())
                                      .Except(validContentIds.AsEnumerable().Select(r => r.Field<string>("Sub Category").ToUpper())).ToList();
                        if (idsNotInA.Count > 0)
                        {
                            DataTable TableC = (from row in dt.AsEnumerable()
                                                join id in idsNotInA
                                                on row.Field<string>("Sub Category").ToUpper() equals id.ToUpper()
                                                select row).CopyToDataTable();
                            getMisMatchRecords.Merge(TableC);
                        }
                        //   dt = validContentIds;
                        ViewState["getMisMatchRecords"] = getMisMatchRecords;
                        if (ViewState["getMisMatchRecords"] != null && getMisMatchRecords.Rows.Count > 0)
                        {
                            lbl_MishMatch.Text = ((DataTable)ViewState["getMisMatchRecords"]).DefaultView.ToTable(true, "Retailer_ID", "card", "ACTIVITY_ORDER_DATE", "transaction_id", "ALLOCATED_POINTS", "order_at_place", "Category", "Sub Category").Rows.Count.ToString();

                        }
                    }
                    //end here

                    TemplateUrl(dt, dtDatabaseColumns, ddltables.SelectedItem.Text, filedestloc, filesourceloc, savedFileAtServer, count);
                    //start here
                    pnlStatus.Visible = true;
                    ddltables.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message.ToString();
            }
            //end here

        }
        protected string ValidateFieldForExcel(DataTable dtDatabaseColumns, string excelColumnValue, string dcExcelColumn, int i, int j)
        {


            string databaseColumnName = dtDatabaseColumns.Rows[0]["Column_Name"].ToString();
            string databaseColumnType = dtDatabaseColumns.Rows[0]["Data_type"].ToString();
            int databaseColumnLength = Convert.ToInt32(dtDatabaseColumns.Rows[0][2].ToString());

            string error = String.Empty;
            try
            {
                if (dcExcelColumn.Trim().ToUpper() == databaseColumnName.Trim().ToUpper())
                {
                    if (databaseColumnType == "nvarchar" && excelColumnValue != "")  // length check
                    {
                        if (databaseColumnName.ToLower() != "retailer_id")
                        {
                            if (excelColumnValue.Length > databaseColumnLength / 2)
                            {
                                error += databaseColumnName + " length must not be greater than " + databaseColumnLength / 2 + " characters.";
                            }
                            if (databaseColumnName.ToLower() == "upload_upto" || databaseColumnName.ToLower() == "upload_date" || databaseColumnName.ToLower() == "month" || databaseColumnName.ToUpper() == "ACTIVITY_ORDER_DATE")
                            {
                                CultureInfo enUS = new CultureInfo("en-US");
                                //  DateTime val;

                                if (!((excelColumnValue.Replace("/", "-").Replace(",", "-")).Contains("-")))
                                {
                                    error += databaseColumnName + "should be in proper date time format(MM/dd/yyyy).";

                                }
                                if (!excelColumnValue.Any(Char.IsDigit))
                                {
                                    error += databaseColumnName + "should be in proper date time format(MM/dd/yyyy).";
                                }
                                //try
                                //{
                                // DateTime dt =   DateTime.FromOADate(double.Parse(cellval));
                                //}
                                //catch(Exception ex)
                                //{
                                //    errorMessage.Text = cellval + "eRROR:" + ex.Message.ToString();
                                //}
                            }
                        }
                        else
                        {
                            if (excelColumnValue.Length > databaseColumnLength / 2)
                            {
                                error += databaseColumnName + " length must not be greater than " + databaseColumnLength + " characters.";
                            }
                            int val;
                            bool result = Int32.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }
                        }
                    }
                    if (databaseColumnType == "float" && excelColumnValue != "")  // length check
                    {

                        double val;
                        bool result = Double.TryParse(excelColumnValue, out val);
                        if (!result)
                        {
                            error += databaseColumnName + " must be a decimal value of max length" + databaseColumnLength;
                        }

                    }

                    if (databaseColumnType == "double" && excelColumnValue != "")
                    {

                        double val;
                        bool result = Double.TryParse(excelColumnValue, out val);
                        if (!result)
                        {
                            error += databaseColumnName + " must be a decimal value of max length" + databaseColumnLength;
                        }

                    }
                    else if (databaseColumnType == "int" && excelColumnValue != "")
                    {


                        int val;
                        bool result = Int32.TryParse(excelColumnValue, out val);
                        if (!result)
                        {
                            error += databaseColumnName + " must be a numeric value.";
                        }

                        if (excelColumnValue.ToString().Length > 10 && databaseColumnName.ToLower() != "year")
                        {
                            error += databaseColumnName + " length must not be greater than " + 10;
                        }
                        else if (databaseColumnName.ToLower() == "year" && excelColumnValue.ToString().Length != 4)
                        {
                            error += databaseColumnName + " length must not be greater than " + 4;
                        }

                    }

                    else if (databaseColumnType == "numeric" && excelColumnValue != "")
                    {

                        if (databaseColumnName.ToLower() != "card" && databaseColumnName.ToLower() != "allocated_points" && databaseColumnName.ToLower() != "points_uploaded")
                        {
                            int val;
                            bool result = Int32.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }

                            if (excelColumnValue.ToString().Length > databaseColumnLength)
                            {
                                error += databaseColumnName + " length must not be greater than " + databaseColumnLength;
                            }
                        }
                        if (databaseColumnName.ToLower() == "card")
                        {
                            long val;
                            bool result = Int64.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }

                            if (excelColumnValue.ToString().Length > 16)
                            {
                                error += databaseColumnName + " length must not be greater than " + (16);
                            }
                        }

                        if (databaseColumnName.ToLower() == "points_uploaded")
                        {
                            long val;
                            bool result = Int64.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }

                            if (excelColumnValue.ToString().Length > 17)
                            {
                                error += databaseColumnName + " length must not be greater than " + (16);
                            }
                        }




                        if (databaseColumnName.ToLower() == "allocated_points" || databaseColumnName.ToLower() == "Points")
                        {
                            long val;
                            bool result = Int64.TryParse(excelColumnValue, out val);
                            if (!result)
                            {
                                error += databaseColumnName + " must be a numeric value.";
                            }

                            if (excelColumnValue.ToString().Length > 15)
                            {
                                error += databaseColumnName + " length must not be greater than " + (15);
                            }
                        }

                    }



                    else if (databaseColumnType == "bigint" && excelColumnValue.Trim() != "")
                    {


                        Int64 val;
                        bool result = Int64.TryParse(excelColumnValue, out val);
                        if (!result)
                        {
                            error += databaseColumnName + " must be a numeric value.";
                        }

                        if (excelColumnValue.ToString().Length > 19)
                        {
                            error += databaseColumnName + " length must not be greater than " + 19;
                        }
                    }

                    else if (databaseColumnType == "datetime" && excelColumnValue.Trim() != "")
                    {

                        CultureInfo enUS = new CultureInfo("en-US");
                        DateTime val;
                        bool result = DateTime.TryParseExact(excelColumnValue, "MM-dd-yy", enUS, DateTimeStyles.None, out val);
                        if (!result)
                        {
                            error += databaseColumnName + "should be in proper date time format(MM-dd-yy).";
                        }

                    }

                    else if (databaseColumnType == "bit" && excelColumnValue.Trim() != "")
                    {
                        string excelColVal = Convert.ToString(excelColumnValue).Trim().ToLower();
                        if (!(excelColVal == "1" || excelColVal == "0"))
                        {
                            error += databaseColumnName + " should be either 1 or 0.";
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                error += databaseColumnName + ex.Message.ToString();
            }

            if (error == "")
                error = "true";
            return error;

        }
        /// <summary>
        /// Modified By: Yogendra Singh
        /// Modified On: 06 March 2017
        /// Bind Table name for bulk upload
        /// </summary>
        protected void BindDatabaseTables()
        {
            try
            {
                DataTable dt = dl.getAllBulkUploadTables();
                dt.Columns.Add("RenameTable").SetOrdinal(0);
                // Start By Yogendra Singh For Upload retailer in SD Login on 6 march 2017
                //if (Session["User_Type"].ToString() == "SD")
                //{
                //    var result = from row in dt.AsEnumerable()
                //                 where row.Field<string>("TableName").Trim() == "upld_Retailer"
                //                 select row;
                //    dt = result.CopyToDataTable();
                //}
                //else
                //{
                    for (int i = 1; i <= dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr["TableName"].ToString() == "upld_Retailer")
                            dr.Delete();
                        dt.AcceptChanges();
                 //   }
                }
                // END By Yogendra Singh For Upload retailer in SD Login on 6 march 2017
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["TableName"].ToString().Trim() == "upld_PointsUploadedDetails")
                    {
                        dr["RenameTable"] = "PointsUploadedDetails";
                        dt.AcceptChanges();
                    }
                    else if (dr["TableName"].ToString().Trim() == "upld_redemption")
                    {
                        dr["RenameTable"] = "Redemption";
                        dt.AcceptChanges();
                    }
                    else if (dr["TableName"].ToString().Trim() == "upld_PointExpiry")
                    {
                        dr["RenameTable"] = "PointExpiry";
                        dt.AcceptChanges();
                    }
                    else if (dr["TableName"].ToString().Trim() == "upld_PayBackOrderDetails")
                    {
                        dr["RenameTable"] = "PayBackOrderDetails";
                        dt.AcceptChanges();
                    }
                    else if (dr["TableName"].ToString().Trim() == "upld_CardDispatchDetail")
                    {
                        dr["RenameTable"] = "CardDispatchDetail";
                        dt.AcceptChanges();
                    }
                    else
                    {
                        string tableName = dr["TableName"].ToString().Trim().Substring(5, dr["TableName"].ToString().Trim().Length - 5);
                        dr["RenameTable"] = tableName;
                        dt.AcceptChanges();
                    }
                }


                if (dt.Rows.Count > 0)
                {
                    ddltables.DataSource = dt;
                    ddltables.DataTextField = "RenameTable";
                    //      ddltables.DataValueField = "UniqueId";
                    ddltables.DataValueField = "TableName";
                    ddltables.DataBind();
                    ddltables.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }

            catch (Exception ex)
            {
                lblNewUploadmsg.Text = ex.Message.ToString();
            }
        }
        //start here
        //error write here void
        public void TemplateUrl(DataTable templatelist, DataTable dt1, string fileName, string destinationfilename, string sourceFileExtension, string savedFileAtServer, int totalrows)
        {
            int ik = 0;
            DataTable dt = templatelist;
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();



            DataTable resultTable = new DataTable();
            DataTable dtError = new DataTable();
            /////more than one column case use below code inside a loop with count of columns
            dtError.Columns.Add("rownumber", typeof(string));
            dtError.Columns.Add("colnumber", typeof(string));
            dtError.Columns.Add("errorcomment", typeof(string));

            //Added for PayBackTable


            int i = 0;
            try
            {
                if (ddltables.SelectedValue != "upld_PayBackOrderDetails" && ddltables.SelectedValue != "upld_CardDispatchDetail")
                {
                    List<DataTable> splitdt = SplitTable(dt, 50);

                    List<DataTable> dtErrorContainer = new List<DataTable>();

                    Task t;
                    var tasks = new ConcurrentBag<Task>();
                    foreach (DataTable item in splitdt)
                    {
                        DataTable dterr = dtError.Clone();
                        t = Task.Factory.StartNew(() => checkdataValidation(item, dt1, dterr, totalrows, i));
                        dtErrorContainer.Add(dterr);
                        tasks.Add(t);
                        t.Wait();
                        i = i + 50;

                    }

                    try
                    {
                        Task.WaitAll(tasks.ToArray());

                        foreach (DataTable dtMerge in dtErrorContainer)
                        {
                            foreach (DataRow drEr in dtMerge.Rows)
                            {
                                dtError.ImportRow(drEr);
                            }
                        }
                        if (dtError.Rows.Count > 13000)
                        {
                            ViewState["datatable"] = GetFirstThreeRowViaLinq(dtError);

                            getDataTable.getData = dtError;
                        }
                        else
                        {
                            ViewState["datatable"] = dtError;
                            getDataTable.getData = dtError;
                        }
                    }

                    catch (AggregateException e)
                    {
                        Console.WriteLine("\nAggregateException thrown with the following inner exceptions:");
                        // Display information about each exception. 
                        foreach (var v in e.InnerExceptions)
                        {
                            if (v is TaskCanceledException)
                                Console.WriteLine("   TaskCanceledException: Task {0}",
                                                  ((TaskCanceledException)v).Task.Id);
                            else
                                Console.WriteLine("   Exception: {0}", v.GetType().Name);
                        }
                        Console.WriteLine();
                    }
                    finally
                    {

                    }
                }

                SendDataToDataBase(dtError, dt, destinationfilename, sourceFileExtension, savedFileAtServer);
            }
            catch (Exception ex)
            {

            }


        }

        //end here

        //get cell value
        private static string GetCellValue(string fileName, string sheetName, string addressName, string val)
        {
            string value = null;
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, false))
                {
                    WorkbookPart wbPart = document.WorkbookPart;

                    // Find the sheet with the supplied name, and then use that Sheet
                    // object to retrieve a reference to the appropriate worksheet.
                    Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                      Where(s => s.Name == sheetName).FirstOrDefault();

                    if (theSheet == null)
                    {
                        throw new ArgumentException("sheetName");
                    }

                    // Retrieve a reference to the worksheet part, and then use its 
                    // Worksheet property to get a reference to the cell whose 
                    // address matches the address you supplied:
                    WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));
                    Cell theCell = wsPart.Worksheet.Descendants<Cell>().
                      Where(c => c.CellReference == addressName).FirstOrDefault();

                    // If the cell does not exist, return an empty string:
                    if (theCell != null)
                    {
                        value = theCell.InnerText;

                        // If the cell represents a numeric value, you are done. 
                        // For dates, this code returns the serialized value that 
                        // represents the date. The code handles strings and Booleans
                        // individually. For shared strings, the code looks up the 
                        // corresponding value in the shared string table. For Booleans, 
                        // the code converts the value into the words TRUE or FALSE.
                        if (theCell.DataType != null && val != "D")
                        {
                            switch (theCell.DataType.Value)
                            {
                                case CellValues.SharedString:
                                    // For shared strings, look up the value in the shared 
                                    // strings table.
                                    var stringTable = wbPart.
                                      GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                                    // If the shared string table is missing, something is 
                                    // wrong. Return the index that you found in the cell.
                                    // Otherwise, look up the correct text in the table.
                                    if (stringTable != null)
                                    {
                                        value = stringTable.SharedStringTable.
                                          ElementAt(int.Parse(value)).InnerText;
                                    }
                                    break;

                                case CellValues.Boolean:
                                    switch (value)
                                    {
                                        case "0":
                                            value = "FALSE";
                                            break;
                                        default:
                                            value = "TRUE";
                                            break;
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return value;
        }

        //get total rows 
        public static int totalRows1(string path)
        {
            int rowsCount = 0;
            using (SpreadsheetDocument myDoc = SpreadsheetDocument.Open(path, false))
            {
                //Get workbookpart
                WorkbookPart workbookPart = myDoc.WorkbookPart;

                //then access to the worksheet part
                IEnumerable<WorksheetPart> worksheetPart = workbookPart.WorksheetParts;

                foreach (WorksheetPart WSP in worksheetPart)
                {
                    //find sheet data
                    IEnumerable<SheetData> sheetData = WSP.Worksheet.Elements<SheetData>();
                    // Iterate through every sheet inside Excel sheet
                    foreach (SheetData SD in sheetData)
                    {
                        IEnumerable<Row> row = SD.Elements<Row>(); // Get the row IEnumerator
                        Console.WriteLine(row.Count()); // Will give you the count of rows
                        rowsCount = row.Count();
                    }

                }

                return rowsCount;

            }
        }

        //delete rows from excell
        public static void DeleteRowFromExcel(string docName, string sheetName, uint rowIndex)
        {
            // Open the document for editing.
            SpreadsheetDocument document = SpreadsheetDocument.Open(docName, true);
            WorkbookPart wbPart = document.WorkbookPart;
            var sheets = wbPart.Workbook.Descendants<Sheet>();
            var sheet = sheets.First();


            var workSheet = ((WorksheetPart)wbPart.GetPartById(sheet.Id)).Worksheet;
            var sheetData = workSheet.Elements<SheetData>().First();
            var rows = sheetData.Elements<Row>().ToList();
            var row = rows.Where(r => r.RowIndex.Value == rowIndex).First();
            row.Hidden = true;
            workSheet.Save();


        }

        public static void AddComments()
        {

            SLDocument sl = new SLDocument();

            sl.SetCellValue(1, 1, "Commenting on comments...");
            sl.SetCellValue(2, 1, "Commenting on comments...");
            sl.SetCellValue(3, 1, "Commenting...");
            sl.SetCellValue(4, 1, "COMMenting...");
            sl.SetCellValue(5, 1, "COMMENTING!");
            sl.SetCellValue(6, 1, "*ding*");

            SLComment comm;

            // this is probably the bare minimum you need to get a comment.
            // Note that if you don't set the position, it will be automatically placed near
            // the cell it's attached to.
            comm = sl.CreateComment();
            comm.SetText("first!!111!1!!1!!");
            sl.InsertComment(2, 6, comm);

            // This simulates the "typical" Excel comment look.
            SLFont font = sl.CreateFont();
            font.SetFont("Tahoma", 9);
            font.Bold = true;
            SLRstType rst = sl.CreateRstType();
            rst.AppendText("Karen:\n", font);
            rst.AppendText("We have a troll! That is so immature...");
            comm = sl.CreateComment();
            comm.SetText(rst);
            sl.InsertComment(2, 10, comm);

            // You don't have to set an author explicitly. SpreadsheetLight will use this property
            // sl.DocumentProperties.Creator
            // if it's set. Otherwise, a default author name will be used. Speaking of which...

            comm = sl.CreateComment();
            comm.Author = "Ben";
            // You don't have to include the author name in the comment if you don't want to.
            // If you do, just follow the above code to set it explicitly.
            comm.SetText("Calm down Karen. We'll take care of that guy.");
            sl.InsertComment(7, 6, comm);

            comm = sl.CreateComment();
            comm.SetText("Why am I so far here? I wanna be with you guys!");
            // Positions depend on the computer's DPI.
            // Th resulting spreadsheet (if you downloaded it) is generated on a 120 DPI monitor,
            // so if you view it on 96 DPI, the positions of all the comments will likely be slightly off.
            // Positions are measured from the top-left cell.
            // This means to put the comment box at the top-left corner of row 16, column 14
            comm.SetPosition(16, 14);
            // You might want to set Visible to true. If invisible, Excel shows the comment
            // close to where it's anchored.
            sl.InsertComment(7, 10, comm);

            comm = sl.CreateComment();
            // widths and heights are measured in points. For convenience you can use
            // SLConvert.FromInchToPoint() if you're using imperial units and
            // SLConvert.FromCentimeterToPoint() if you're using metric units.
            comm.Width = 240;
            comm.Height = 180;
            comm.SetText("Can't eat anymore... I'm bloated...");
            sl.InsertComment(12, 6, comm);

            // in case you want to fit the contents snugly
            comm = sl.CreateComment();
            comm.SetText("These jeans fit me perfectly!");
            comm.AutoSize = true;
            sl.InsertComment(12, 10, comm);

            rst = sl.CreateRstType();
            font = sl.CreateFont();
            font.SetFont("Harrington", 16);
            rst.AppendText("Don't envy", font);
            font = sl.CreateFont();
            font.Bold = true;
            font.Italic = true;
            font.Strike = true;
            font.VerticalAlignment = VerticalAlignmentRunValues.Superscript;
            rst.AppendText(" me because", font);
            font = sl.CreateFont();
            font.Underline = UnderlineValues.Single;
            font.SetFontThemeColor(SLThemeColorIndexValues.Accent4Color);
            rst.AppendText(" I've got style...", font);
            rst.AppendText(" and you don't.");
            comm = sl.CreateComment();
            comm.SetText(rst);
            sl.InsertComment(17, 6, comm);

            // comment text alignment
            comm = sl.CreateComment();
            comm.HorizontalTextAlignment = SLHorizontalTextAlignmentValues.Right;
            comm.VerticalTextAlignment = SLVerticalTextAlignmentValues.Center;
            comm.SetText("Stop manhandling me!");
            sl.InsertComment(17, 10, comm);

            // the top-down orientation
            comm = sl.CreateComment();
            comm.Orientation = SLCommentOrientationValues.TopDown;
            // set larger size so all the comment text can be displayed.
            comm.Width = 160;
            comm.Height = 120;
            comm.SetText("I read like those ancient Chinese texts, yah?");
            sl.InsertComment(22, 6, comm);

            // another orientation
            comm = sl.CreateComment();
            comm.Orientation = SLCommentOrientationValues.Rotated270Degrees;
            comm.SetText("I'm getting dizzy...");
            sl.InsertComment(22, 10, comm);

            // when you want to show comments. The default is to hide them.
            comm = sl.CreateComment();
            comm.Visible = true;
            comm.SetText("How come everyone's got the invisibility superpower?");
            sl.InsertComment(27, 6, comm);

            // when you want to style the lines surrounding the comment box
            comm = sl.CreateComment();
            comm.LineColor = System.Drawing.Color.Coral;
            comm.LineStyle = DocumentFormat.OpenXml.Vml.StrokeLineStyleValues.ThickBetweenThin;
            // this is in points
            comm.LineWeight = 5;
            comm.SetText("I've got fancy outlines.");
            sl.InsertComment(27, 10, comm);

            // for another fancy outline
            comm = sl.CreateComment();
            comm.SetDashStyle(SLDashStyleValues.LongDashDotDot);
            comm.SetText("Do I look like Morse code to you?");
            sl.InsertComment(32, 6, comm);

            // for when you don't want a shadow for the comment box
            comm = sl.CreateComment();
            comm.HasShadow = false;
            comm.SetText("Erhmahgerd! I got no shadow! Who did this to me?");
            sl.InsertComment(32, 10, comm);

            // for when you want a different shadow colour.
            comm = sl.CreateComment();
            comm.ShadowColor = System.Drawing.Color.HotPink;
            rst = sl.CreateRstType();
            rst.AppendText("You think ");
            rst.AppendText(" you've", new SLFont() { Italic = true });
            rst.AppendText(" got a problem? I got a pink shadow. Who has ");
            rst.AppendText("pink", new SLFont() { Italic = true, FontColor = System.Drawing.Color.HotPink });
            rst.AppendText(" shadows?");
            comm.SetText(rst);
            sl.InsertComment(37, 6, comm);

            // Next comes the background fill section. Note that the Fill property is repurposed
            // from somewhere else, and will work as intended most of the time.
            // This is a limitation of the underlying VML properties, and not SpreadsheetLight.
            // For instance, the actual colour of accent colours is captured. But if you change
            // themes (and thus accent colours), the colour you used won't change automatically.

            // for when you don't want a background fill colour
            // This probably work better if you customise the shadow colour too.
            comm = sl.CreateComment();
            comm.Fill.SetNoFill();
            comm.SetText("My life is so empty...");
            sl.InsertComment(37, 10, comm);

            // for an automatic background colour. This is probably just white.
            comm = sl.CreateComment();
            comm.Fill.SetAutomaticFill();
            comm.SetText("Ooh this cup just filled up by itself! ... Can I have chocolate instead?");
            sl.InsertComment(42, 6, comm);

            // the default colour is #ffffe1
            comm = sl.CreateComment();
            // 20% transparency
            comm.Fill.SetSolidFill(System.Drawing.Color.LightSkyBlue, 20);
            comm.SetText("The sky's the limit!");
            sl.InsertComment(42, 10, comm);

            // linear gradients
            comm = sl.CreateComment();
            // 40% transparency on the first gradient point
            comm.GradientFromTransparency = 40;
            // 80% transparency on the last gradient point
            comm.GradientToTransparency = 80;
            // 45 degrees, so gradient is from top-left to bottom-right
            comm.Fill.SetLinearGradient(SpreadsheetLight.Drawing.SLGradientPresetValues.Rainbow, 45);
            comm.SetText("I'm a unicorn! I've got rainbows coming out the wazoo!");
            sl.InsertComment(47, 6, comm);

            // path gradients
            comm = sl.CreateComment();
            comm.Fill.SetPathGradient(SpreadsheetLight.Drawing.SLGradientPresetValues.Ocean);
            comm.SetText("My gradients are so squarish...");
            sl.InsertComment(47, 10, comm);

            // fancier gradients
            comm = sl.CreateComment();
            comm.Fill.SetRectangularGradient(SpreadsheetLight.Drawing.SLGradientPresetValues.LateSunset, SpreadsheetLight.Drawing.SLGradientDirectionValues.CenterToBottomLeftCorner);
            // for the purposes of setting gradients, the following
            //comm.Fill.SetRadialGradient(SpreadsheetLight.Drawing.SLGradientPresetValues.LateSunset, SpreadsheetLight.Drawing.SLGradientDirectionValues.CenterToBottomLeftCorner);
            // is the same as SetRectangularGradient(). The technical explanation is that VML don't
            // support circular gradients...
            comm.SetText("Oh stop complaining... my gradients aren't that hot either.");
            sl.InsertComment(52, 6, comm);

            // pattern fills!
            comm = sl.CreateComment();
            comm.Fill.SetPatternFill(DocumentFormat.OpenXml.Drawing.PresetPatternValues.Wave, System.Drawing.Color.GhostWhite, System.Drawing.Color.LightBlue);
            comm.SetText("I'm riding the wave!");
            sl.InsertComment(52, 10, comm);

            // picture backgrounds!
            comm = sl.CreateComment();
            // left, right, top and bottom offsets. It's recommended that you just leave them as zero.
            // This particular method overload will stretch the picture. And given all-zero offsets,
            // it's effectively filling up the whole comment box.
            // The last zero is the transparency.
            comm.Fill.SetPictureFill("mandelbrot.png", 0, 0, 0, 0, 0);
            comm.SetText("I've got a fractal background!");
            sl.InsertComment(57, 6, comm);

            comm = sl.CreateComment();
            // this is one of those methods that don't quite match completely...
            // the first 2 zeroes are OffsetX and OffsetY, which aren't used, so just set them as zero.
            // 33 means 33%, so the picture will be tiled approximately 3 times (100 / 33) horizontally.
            // 50 means 50%, so it will be tiled 2 times (100 / 50) vertically.
            // You can ignore RectangleAlignmentValues and TileFlipValues (for now?)
            // The last zero is the transparency.
            comm.Fill.SetPictureFill("julia.png", 0, 0, 33, 50,
                DocumentFormat.OpenXml.Drawing.RectangleAlignmentValues.Bottom,
                DocumentFormat.OpenXml.Drawing.TileFlipValues.None,
                0);
            comm.SetText("Well, I've got Julia. *wink wink*");
            sl.InsertComment(57, 10, comm);

            sl.SaveAs("CellComments.xlsx");


        }
        public void SetErrorStyleSheet(string Path, string SheetName, DataTable dt)
        {
            try
            {
                SLDataValidation dv;
                using (SLDocument sl = new SLDocument(Path, SheetName))
                {
                    SLStyle style4 = sl.CreateStyle();
                    style4.SetFontColor(System.Drawing.Color.Red);
                    style4.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Yellow, System.Drawing.Color.Yellow);




                    SLStyle styleheader = sl.CreateStyle();
                    styleheader.SetFontColor(System.Drawing.Color.Black);
                    //styleheader.SetFontBold(true);
                    styleheader.Protection.Locked = false;
                    styleheader.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightBlue, System.Drawing.Color.LightBlue);
                    if (ddltables.SelectedValue == "upld_PointsUploadedDetails")
                    {


                        dv = sl.CreateDataValidation("E1", "F1");
                        dv.SetInputMessage("Rule", "Should be in proper date time format(MM/DD/YYYY).");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "SD field will accept string values upto 8 characters");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "  Batch field will accept alphanumeric values upto 10 characters with special characters i.e. '_' and  '/' included");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("G1", "G1");
                        dv.SetInputMessage("Rule", "Year field will be accept numeric[4]");
                        sl.AddDataValidation(dv);

                        //dv = sl.CreateDataValidation("H1", "H1");
                        //dv.SetInputMessage("Rule", "Towards field will accept string values upto 12 characters");
                        //sl.AddDataValidation(dv);

                        //dv = sl.CreateDataValidation("I1", "I1");
                        //dv.SetInputMessage("Rule", "Detail field will include string value upto 15 characters");
                        //sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("J1", "J1");
                        dv.SetInputMessage("Rule", "Retailer ID is numeric[9]");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("K1", "K1");
                        dv.SetInputMessage("Rule", " Points uploaded is numeric[17]");
                        sl.AddDataValidation(dv);


                        dv = sl.CreateDataValidation("B1", "B1");
                        dv.SetInputMessage("Rule", " Record ID will be 10 digit code, auto-generated as: created date (DDMMYY) + serial number starting from 0001 when ACTION is not A.");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("A1", "A1");
                        dv.SetInputMessage("Rule", "Insert Only 'A','M','D' Other Wise this record will not be inserted.");
                        sl.AddDataValidation(dv);

                    }
                    else if (ddltables.SelectedValue == "upld_redemption")
                    {
                        dv = sl.CreateDataValidation("B1", "B1");
                        dv.SetInputMessage("Rule", " Record ID will be 10 digit code, auto-generated as: created date (DDMMYY) + serial number starting from 0001 when ACTION is not A.");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("A1", "A1");
                        dv.SetInputMessage("Rule", "Insert Only 'A','M','D' Other Wise this record will not be inserted.");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "Retailer ID field will accept  values upto 9 digit");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "Card field will accept  values upto 16 digit");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("E1", "E1");
                        dv.SetInputMessage("Rule", "Allocated Point field will accept  values upto 15 digit");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("F1", "F1");
                        dv.SetInputMessage("Rule", "Order At Place field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("G1", "G1");
                        dv.SetInputMessage("Rule", "Categery field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);
                    }
                    else if (ddltables.SelectedValue == "upld_PointExpiry")
                    {
                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "Retailer ID field will accept  values upto 9 digit");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "Date should be MM/DD/YYYY format");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("E1", "E1");
                        dv.SetInputMessage("Rule", "Point field will accept  values upto 15 digit");
                        sl.AddDataValidation(dv);
                    }
                    else if (ddltables.SelectedValue == "upld_RetailerSegmentation")
                    {
                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "Retailer ID field will accept  values upto 9 digit");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "YEAR should be YYYY format and also nummeric type.");
                        sl.AddDataValidation(dv);

                    }
                    else if (ddltables.SelectedValue == "upld_CategoryRedemption")
                    {
                        dv = sl.CreateDataValidation("D1", "D1");
                        dv.SetInputMessage("Rule", "Card field will accept  values upto 16 digit");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("E1", "E1");
                        dv.SetInputMessage("Rule", "ACTIVITY_ORDER_DATE should be MM/DD/YYYY format");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("G1", "G1");
                        dv.SetInputMessage("Rule", " ALLOCATED_POINTS uploaded is numeric[17]");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("H1", "H1");
                        dv.SetInputMessage("Rule", "Order At Place field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);
                        dv = sl.CreateDataValidation("I1", "I1");
                        dv.SetInputMessage("Rule", "Categery field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);

                        dv = sl.CreateDataValidation("J1", "J1");
                        dv.SetInputMessage("Rule", "Sub Categery field will accept  values upto 20 char");
                        sl.AddDataValidation(dv);


                        dv = sl.CreateDataValidation("C1", "C1");
                        dv.SetInputMessage("Rule", "Retailer ID is numeric[9]");
                        sl.AddDataValidation(dv);

                    }
                    else
                    {

                    }
                    sl.SetRowStyle(1, styleheader);

                    Parallel.For(0, dt.Rows.Count, new ParallelOptions { MaxDegreeOfParallelism = 1000 }, i =>
                    {
                        sl.SetCellStyle(Convert.ToInt32(dt.Rows[i]["rownumber"]) + 1, Convert.ToInt32(dt.Rows[i]["colnumber"]), style4);
                    }); // Par

                    sl.SaveAs(Path);
                }
            }
            catch (Exception ex)
            {

            }
        }


        //split a datatable into multipletable
        private static List<DataTable> SplitTable(DataTable originalTable, int batchSize)
        {
            List<DataTable> tables = new List<DataTable>();
            int i = 0;
            int j = 1;
            DataTable newDt = originalTable.Clone();
            newDt.TableName = "Table_" + j;
            newDt.Clear();
            foreach (DataRow row in originalTable.Rows)
            {
                DataRow newRow = newDt.NewRow();
                newRow.ItemArray = row.ItemArray;
                newDt.Rows.Add(newRow);
                i++;
                if (i == batchSize)
                {
                    tables.Add(newDt);
                    j++;
                    newDt = originalTable.Clone();
                    newDt.TableName = "Table_" + j;
                    newDt.Clear();
                    i = 0;
                }
            }
            if (newDt.Rows.Count > 0 && i != 0)
            {
                tables.Add(newDt);
                j++;
                newDt = originalTable.Clone();
                newDt.TableName = "Table_" + j;
                newDt.Clear();
                i = 0;
            }
            return tables;
        }
        //check validation
        public void checkdataValidation(DataTable dt, DataTable dt1, DataTable dtError, int totalrows, int rows)
        {
            try
            {
                DataTable resultTable = new DataTable();

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {

                    for (int j = 0; j <= dt.Columns.Count - 1; j++)
                    {
                        var ColumnName = dt.Columns[j].ColumnName.ToString();

                        string expression;
                        DataTable resultDataTable = new DataTable();
                        expression = "Column_Name = '" + ColumnName.Trim() + "'";
                        DataRow[] foundRows;

                        // Use the Select method to find all rows matching the filter.
                        try
                        {
                            foundRows = dt1.Select(expression);
                        }
                        catch (Exception ex)
                        {
                            foundRows = null;
                        }
                        ////////////add new row after adding columns
                        if (foundRows.Count() > 0)
                        {
                            resultDataTable = foundRows.CopyToDataTable();
                            if (ColumnName == resultDataTable.Rows[0][0].ToString())
                            {
                                if (dt.Rows[i][j - 1].ToString().ToLower() == "a" && ColumnName == "Record_ID")
                                {
                                    dt.Rows[i][j] = "";
                                }
                                else
                                {
                                    if (!(ColumnName.ToString().Contains("CreatedDate") || ColumnName.ToString().Contains("ModifiedDate") || ColumnName.ToString().Contains("Status")))
                                    {
                                        string result = "";
                                        if (dt.Rows[i][j].ToString() != "")
                                        {
                                            result = ValidateFieldForExcel(resultDataTable, dt.Rows[i][j].ToString(), ColumnName, i + 1, j);

                                            if (result != "true")
                                            {
                                                ik = ik + 1;
                                                DataRow dr1 = dtError.NewRow();
                                                dr1["rownumber"] = i + 1 + rows;
                                                dr1["colnumber"] = j + 1;
                                                dr1["errorcomment"] = result;
                                                dtError.Rows.Add(dr1.ItemArray);
                                            }

                                            ViewState["datatable"] = dtError;
                                            lbl_TotalRecords.Text = (totalrows - 1).ToString();
                                            resultTable = dtError;

                                            lbl_InvalidRecords.Text = resultTable.DefaultView.ToTable(true, "rownumber").Rows.Count.ToString();
                                            // lbl_InvalidRecords.Text = resultTable.Rows.Count.ToString();
                                            lbl_InvalidCells.Text = ik.ToString();
                                            lbl_ValidaRecords.Text = (totalrows - 1 - resultTable.DefaultView.ToTable(true, "rownumber").Rows.Count).ToString();
                                            // lbl_ValidaRecords.Text = (totalrows - resultTable.Rows.Count).ToString();
                                        }
                                        else
                                        {
                                            result = "Please enter the values in others columns except the 'Record ID','Created date',' Modified Date' and 'Status'.";
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }
        //Insering data to database
        public void SendDataToDataBase(DataTable dtError, DataTable dt, string destinationfilename, string sourceFileExtension, string savedFileAtServer)
        {
            Int32 getRowsCount = dt.Rows.Count;
            if (dtError.Rows.Count == 0)
            {
                try
                {
                    if (ddltables.SelectedItem.Text == "CategoryRedemption")
                    {
                        dt = (DataTable)ViewState["validContentIds"];
                    }
                    string destinationTableName = ddltables.SelectedValue + "_Staging";
                    DataTable result = dt.Clone();
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["Action"].ToString().ToLower() == "a" || dr["Action"].ToString().ToLower() == "m" || dr["Action"].ToString().ToLower() == "d")
                            result.Rows.Add(dr.ItemArray);
                    }
                    getRowsCount = result.Rows.Count;
                    //for removing space
                    for (int i = 0; i < result.Rows.Count; i++)
                        for (int j = 0; j < result.Columns.Count; j++)
                        {
                            string o = Convert.ToString(result.Rows[i][j]);
                            if (o.Length > 0)
                            {
                                if (o.Trim().Length == 0)
                                {
                                    result.Rows[i][j] = null;
                                    if (result.Columns[j].ColumnName == "Retailer_ID" || result.Columns[j].ColumnName == "card" || result.Columns[j].ColumnName == "ALLOCATED_POINTS" || result.Columns[j].ColumnName == "Year" || result.Columns[j].ColumnName == "Points_Uploaded")
                                    {
                                        result.Rows[i][j] = 0;
                                    }
                                    result.AcceptChanges();
                                }
                            }
                            else
                            {
                                if (result.Columns[j].ColumnName == "Retailer_ID" || result.Columns[j].ColumnName == "card" || result.Columns[j].ColumnName == "ALLOCATED_POINTS" || result.Columns[j].ColumnName == "Year" || result.Columns[j].ColumnName == "Points_Uploaded")
                                {
                                    result.Rows[i][j] = 0;
                                }
                                result.AcceptChanges();
                            }
                            //if you want to get the string
                            //string s = o = dt.Rows[i].ItemArray[j].ToString();
                        }

                    //   end here
                    if (result.Rows.Count > 0)
                    {
                        xlColNoLogger = "creating table copy starts.";
                        xlRowNoLogger = "";
                        dl.createTableCopy(ddltables.SelectedValue); // creates a staging table
                        xlColNoLogger = "bulk upload for table starts.";
                        xlRowNoLogger = "";
                        if (result.Columns.Contains("Status"))
                        {
                            result.Columns.Remove("Status");
                            result.AcceptChanges();
                        }
                        if (result.Columns.Contains("CreatedDate"))
                        {
                            result.Columns.Remove("CreatedDate");
                            result.AcceptChanges();
                        }
                        if (result.Columns.Contains("ModifiedDate"))
                        {
                            result.Columns.Remove("ModifiedDate");
                            result.AcceptChanges();
                        }
                        if (result.Columns.Contains("Record_ID"))
                        {
                            foreach (DataRow dr in result.Rows)
                            {

                                if (dr["Record_ID"] != DBNull.Value)
                                {

                                    string strRecordID = dr["Record_ID"].ToString();
                                    if (strRecordID.Length > 12)
                                    {
                                        dr["Record_ID"] = strRecordID.Substring(12, strRecordID.Trim().Length - 12);
                                    }

                                }
                            }
                        }
                        dl.BulkUpload(result, destinationTableName); // bulk uploading of data
                        xlColNoLogger = "copy data from staging starts.";
                        xlRowNoLogger = "";
                        dl.CopyDataFromStagingToUpld(ddltables.SelectedValue); // shifting data from staging to orignal and deleting staging table.


                        lbl_TotalRecords.Text = (result.Rows.Count).ToString();

                        if (ViewState["datatable"] != null)
                        {
                            lbl_InvalidRecords.Text = getDataTable.getData.DefaultView.ToTable(true, "rownumber").Rows.Count.ToString();
                            lbl_ValidaRecords.Text = (result.Rows.Count - getDataTable.getData.DefaultView.ToTable(true, "rownumber").Rows.Count).ToString();
                        }
                        else
                        {
                            lbl_InvalidRecords.Text = "0";
                            lbl_ValidaRecords.Text = (result.Rows.Count - 0).ToString();
                            lbl_InvalidCells.Text = "0";
                            lblMessage.Text = "Uploaded successfully!";
                            lblMessage.Style.Add("Color", "Green");
                            btnReport.Enabled = false;

                        }
                        // lbl_InvalidCells.Text = ik.ToString();




                    }
                    lbl_TotalRecords.Text = (result.Rows.Count + Convert.ToInt16(lbl_MishMatch.Text)).ToString();
                    lbl_InvalidRecords.Text = "0";
                    lbl_ValidaRecords.Text = (result.Rows.Count - 0).ToString();
                    lbl_InvalidCells.Text = "0";
                    lblMessage.Text = "Uploaded successfully!";
                    lblMessage.Style.Add("Color", "Green");
                    btnReport.Enabled = false;

                    if (ViewState["FileLocation"] != null)
                    {
                        xlColNoLogger = "excel being moved to archieve";
                        xlRowNoLogger = "";
                        DirectoryInfo diTempArchive = new DirectoryInfo(Server.MapPath("~/BulkUpload/Archive"));
                        if (!diTempArchive.Exists)
                            diTempArchive.Create();
                        string fileLocation = Convert.ToString(ViewState["FileLocation"]);
                        string sourcePath = Path.GetDirectoryName(fileLocation);
                        string targetPath = Server.MapPath("~") + "BulkUpload\\Archive";
                        // Use Path class to manipulate file and directory paths.
                        string sourceFile = Path.Combine(sourcePath, destinationfilename + sourceFileExtension);
                        string destFile = Path.Combine(targetPath, destinationfilename + sourceFileExtension);

                        // To copy a folder's contents to a new location:
                        // Create a new target folder, if necessary.
                        if (!System.IO.Directory.Exists(targetPath))
                        {
                            System.IO.Directory.CreateDirectory(targetPath);
                        }

                        // To copy a file to another location and 
                        // overwrite the destination file if it already exists.
                        System.IO.File.Copy(sourceFile, destFile, true);

                    }
                }
                catch (Exception ex)
                {
                    dl.DeleteStagingTable();
                    lblMessage.Text = ex.Message;
                    lblMessage.Style.Add("Color", "Red");
                    LogUtility.SaveLogEntry(ex.Message + " " + "Row No : " + xlRowNoLogger + ", Col No :" + xlColNoLogger + " in excel " + savedFileAtServer + " for table " + ddltables.SelectedItem.Text);

                    btnReport.Enabled = false;
                    //  lblMessage.Text = "Excel validation failed!";
                    //  lblMessage.Style.Add("Color", "Red");

                    lbl_TotalRecords.Text = "";


                    lbl_InvalidRecords.Text = "";
                    lbl_ValidaRecords.Text = "";
                    lbl_InvalidCells.Text = "";

                }

            }
            else
            {
                //    SetErrorStyleSheet(savedFileAtServer, "Sheet1", dtError);
                if (ViewState["datatable"] != null)
                {
                    btnReport.Enabled = true;
                    lblMessage.Text = "Excel validation failed!";
                    lblMessage.Style.Add("Color", "Red");

                    lbl_TotalRecords.Text = getRowsCount.ToString();


                    lbl_InvalidRecords.Text = getDataTable.getData.DefaultView.ToTable(true, "rownumber").Rows.Count.ToString();
                    lbl_ValidaRecords.Text = (getRowsCount - getDataTable.getData.DefaultView.ToTable(true, "rownumber").Rows.Count).ToString();
                }

            }
        }
        //remove null rows
        public static void RemoveNullColumnFromDataTable(DataTable dt, string str)
        {
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                if (str == "upld_PointsUploadedDetails")
                {
                    if (dt.Rows[i]["SD"] == DBNull.Value && dt.Rows[i]["Batch"] == DBNull.Value && dt.Rows[i]["Upload_Upto"] == DBNull.Value && dt.Rows[i]["Upload_Date"] == DBNull.Value && dt.Rows[i]["Year"] == DBNull.Value && dt.Rows[i]["Towards"] == DBNull.Value && dt.Rows[i]["Detail"] == DBNull.Value && dt.Rows[i]["Retailer_ID"] == DBNull.Value && dt.Rows[i]["Points_Uploaded"] == DBNull.Value)
                    {

                        dt.Rows[i].Delete();
                    }
                    if (dt.Rows[i]["SD"] == DBNull.Value && dt.Rows[i]["Batch"] == DBNull.Value && dt.Rows[i]["Upload_Upto"] == DBNull.Value && dt.Rows[i]["Upload_Date"] == DBNull.Value && Convert.ToString(dt.Rows[i]["Year"]) == "0" && dt.Rows[i]["Towards"] == DBNull.Value && dt.Rows[i]["Detail"] == DBNull.Value && Convert.ToString(dt.Rows[i]["Retailer_ID"]) == "0" && Convert.ToString(dt.Rows[i]["Points_Uploaded"]) == "0")
                    {

                        dt.Rows[i].Delete();
                    }
                }
                else if (str == "upld_redemption")
                {
                    if (Convert.ToString(dt.Rows[i]["card"]) == "0" && Convert.ToString(dt.Rows[i]["ALLOCATED_POINTS"]) == "0" && dt.Rows[i]["order_at_place"] == DBNull.Value && dt.Rows[i]["Category"] == DBNull.Value && dt.Rows[i]["Retailer_ID"] == DBNull.Value)
                    {
                        dt.Rows[i].Delete();

                    }
                    if (dt.Rows[i]["card"] == DBNull.Value && dt.Rows[i]["ALLOCATED_POINTS"] == DBNull.Value && dt.Rows[i]["order_at_place"] == DBNull.Value && dt.Rows[i]["Category"] == DBNull.Value && Convert.ToString(dt.Rows[i]["Retailer_ID"]) == "0")
                    {
                        dt.Rows[i].Delete();

                    }
                }
                else
                {
                    if (dt.Rows[i]["Points"] == DBNull.Value && dt.Rows[i]["month"] == DBNull.Value && dt.Rows[i]["Retailer_ID"] == DBNull.Value)
                    {
                        dt.Rows[i].Delete();

                    }
                    if (dt.Rows[i]["Points"] == DBNull.Value && dt.Rows[i]["month"] == DBNull.Value && dt.Rows[i]["Retailer_ID"] == DBNull.Value)
                    {
                        dt.Rows[i].Delete();

                    }
                }
            }
            dt.AcceptChanges();
        }

        private static DataTable GetFirstThreeRowViaLinq(DataTable dataTable)
        {
            return dataTable.Rows.Cast<System.Data.DataRow>().Take(13000).CopyToDataTable();
        }


        private static DataTable GetSkipThreeRowViaLinq(DataTable dataTable)
        {
            return dataTable.Rows.Cast<System.Data.DataRow>().Skip(13000).Take(13000).CopyToDataTable();
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            lbl_TotalRecords.Text = "0";
            lbl_InvalidRecords.Text = "0";
            lbl_ValidaRecords.Text = "0";
            lbl_InvalidCells.Text = "0";
            lblMessage.Text = "";
            lblMessage.Style.Add("Color", "Green");
            btnReport.Visible = true;
            btnUnMatchRecord.Visible = false;
            btnReport.Enabled = false;
            lbl_MishMatch.Text = "0";
        }

        public void SaveDataInPayBackTable()
        {

        }



        protected void btnUnMatchRecord_Click(object sender, EventArgs e)
        {
            try
            {
                string destinationServerFileName = DateTime.Now.ToString("yyyyMMdd_HHmm") + "_" + ddltables.SelectedItem.Text;
                string savedFileAtServer = Server.MapPath("~") + "\\BulkUpload\\InProgress" + "\\" + destinationServerFileName + ".xlsx";
                if (ddltables.SelectedItem.Text == "CategoryRedemption")
                {
                    if (ViewState["getMisMatchRecords"] != null)
                    {

                        DataTable uniqueCols = ((DataTable)ViewState["getMisMatchRecords"]).DefaultView.ToTable(true, "Retailer_ID", "Record_ID", "card", "ACTIVITY_ORDER_DATE", "transaction_id", "ALLOCATED_POINTS", "order_at_place", "Category", "Sub Category", "CreatedDate", "ModifiedDate", "Status");
                        uniqueCols.Columns.Add("Action").SetOrdinal(0);
                        uniqueCols.AcceptChanges();
                        int count = uniqueCols.Rows.Count;
                        if (((DataTable)ViewState["validContentIds"]).Rows.Count > 0)
                        {
                            uniqueCols.Merge((DataTable)ViewState["validContentIds"]);
                            uniqueCols.Columns["Action"].SetOrdinal(0);

                        }
                        uniqueCols.Columns["Record_ID"].SetOrdinal(1);

                        uniqueCols.TableName = ddltables.SelectedItem.Text;
                        foreach (DataRow dr in uniqueCols.Rows)
                        {
                            if (dr["Action"] == DBNull.Value)
                            {
                                dr["Action"] = "A";
                            }
                            uniqueCols.AcceptChanges();
                        }
                        ExcelPackageExtensions.ExportDataSet(uniqueCols, savedFileAtServer);
                        lbl_MishMatch.Text = uniqueCols.Rows.Count.ToString();
                        SetColourCell(savedFileAtServer, ddltables.SelectedItem.Text + ".xlsx", count);
                    }
                    byte[] byteArray = File.ReadAllBytes(savedFileAtServer);

                    // Do work here
                    Response.AppendHeader("content-disposition", "attachment; filename=" + destinationServerFileName + ".xlsx");
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BinaryWrite(byteArray);
                    Response.End();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void SetColourCell(string Path, string SheetName, int count)
        {
            SLDataValidation dv;
            using (SLDocument sl = new SLDocument(Path, SheetName))
            {
                SLStyle style4 = sl.CreateStyle();
                style4.SetFontColor(System.Drawing.Color.Red);
                style4.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Yellow, System.Drawing.Color.Yellow);




                SLStyle styleheader = sl.CreateStyle();
                styleheader.SetFontColor(System.Drawing.Color.Black);
                //styleheader.SetFontBold(true);
                styleheader.Protection.Locked = false;
                styleheader.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightBlue, System.Drawing.Color.LightBlue);
                Parallel.For(0, count, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
                {
                    sl.SetCellStyle(i + 2, 9, style4);
                    sl.SetCellStyle(i + 2, 10, style4);
                }); // Par

                sl.SaveAs(Path);
            }
        }
        /// <summary>
        /// Created By: Yogendra Singh
        /// CreatedON: 08 Mar 2017
        /// Download the Incorrect Uploaded record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btndownloadreport_Click(object sender, EventArgs e)
        {
            DataTable dt = dl.GetRetailerwrongData();
            clsSpeadSheet objSprdSheet = new clsSpeadSheet();

            byte[] ms = objSprdSheet.TemplateUrlPoints(dt, "Retailer").ToArray();


            HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=RetailerUploadReport.xlsx");
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.BinaryWrite(ms);
        }


    }
}