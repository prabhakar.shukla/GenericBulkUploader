﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeneriCExcelOperations.Models
{
    public class clsBindDropDownListPara
    {
        public string TableName { get; set; }
        public string TableValue { get; set; }
        public string key { get; set; }
    }
}