﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

/****************************************************************************
 Purpose        :   To Create Connection String... its a singleton class
 ****************************************************************************/

namespace DataAccessLayer
{
    /// <summary>
    /// This class is used for making connection to database.
    /// </summary>
    public class DBConnection
    {
        private static string DSDDatabaseConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CONN_RC_IN"].ConnectionString;

      //  private DBConnection() { }
        /// <summary>
        /// Getting the connection value from Config file.
        /// </summary>
        public static  string Connection
        {
            get { return DSDDatabaseConnectionString; }
        }
    }
      

}






