﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using DataAccessLayer;

/// <summary>
/// Summary description for Admin_DLL
/// </summary>
public class Admin_DLL
{
 #region Variable Declarations
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;
    DataSet ds;
    DataTable dt;
    public bool[] Column_check;
    string message = string.Empty;
    int rowAffected = 0;
    bool IsTrue = false;
    int value;
    public static bool IsValidPass = false;
    
    public Admin_DLL() {
        string ConnectioString = Convert.ToString(DBConnection.Connection);
        con = new SqlConnection(ConnectioString);
      }
    #endregion

    #region Common Methods to Get DataSet/DataTable/ExecuteNonQuery
    /// <summary>
    /// Execute the SQL String Query and return single value of first row and first column.
    /// </summary>
    /// <param name="strQuery"></param>
    /// <returns></returns>
    public object DataExecuteScaler(string strQuery) {
        object getValue;
        try {
            cmd = new SqlCommand(strQuery, con);
            cmd.CommandTimeout = 200;
            con.Open();
            getValue = cmd.ExecuteScalar();
        }
        catch (Exception) {
            throw;
        }
        finally {
            con.Close();
        }
        return getValue;
    }

    /// <summary>
    /// Execute the SQL String Query and return the number of affetced rows.
    /// </summary>
    public int DataManipulation(string strQuery) {
        try {
            cmd = new SqlCommand(strQuery, con);
            cmd.CommandTimeout = 200;
            con.Open();
            rowAffected = cmd.ExecuteNonQuery();
        }
        catch (Exception) {
            throw;
        }
        finally {
            con.Close();
        }
        return rowAffected;
    }

    /// <summary>
    /// Execute the SQL Command Query and return the number of affetced rows.
    /// </summary>
    private int DataManipulation(SqlCommand cmdQuery) {
        try {
            cmdQuery.Connection = con;
            cmdQuery.CommandTimeout = 200;
            con.Open();
            rowAffected = cmdQuery.ExecuteNonQuery();
        }
        catch (Exception) {
            throw;
        }
        finally {
            con.Close();
        }
        return rowAffected;
    }

    /// <summary>
    /// Execute the SQL String Query and return the Data Table.
    /// </summary>
    public DataTable GetDataTable(string strQuery) {
        da = new SqlDataAdapter(strQuery, con);
        dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
  
    /// <summary>
    /// Execute the SQL Command Query and return the Data Table.
    /// </summary>
    private DataTable GetDataTable(SqlCommand cmdQuery) {
        cmdQuery.CommandTimeout = 480;
        da = new SqlDataAdapter(cmdQuery);
        dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
   
    /// <summary>
    /// Execute the SQL String Query and return the DataSet.
    /// </summary>
    public DataSet GetDataSet(string strQuery) {
        da = new SqlDataAdapter(strQuery, con);
        ds = new DataSet();     
        da.Fill(ds);
        return ds;
    }

    /// <summary>
    /// Execute the SQL Command Query and return the DataSet.
    /// </summary>
    private DataSet GetDataSet(SqlCommand cmdQuery) {
        da = new SqlDataAdapter(cmdQuery);
        ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    /// <summary>
    /// Get Column names
    /// </summary>
    private bool Header_compare(DataTable XLdt, DataTable DBdt) {
        bool check = false;
        int count = 0;
        Column_check = new bool[XLdt.Columns.Count];
        if (DBdt.Columns.Count > 0) {
            for (int i = 0; i < DBdt.Columns.Count; i++) {
                try {
                    if (XLdt.Columns[i].Caption.ToUpper() == DBdt.Columns[i].Caption.ToUpper()) {
                        Column_check[i] = true;
                        count = count + 1;
                    }
                    else {
                        Column_check[i] = false;
                    }
                }
                catch (Exception ex) {
                    check = false;
                }
            }
        }
        if (count == XLdt.Columns.Count) {
            check = true;
        }
        return check;
    }

    /// <summary>
    /// Audit Log Entry
    /// </summary>
    /// <returns></returns>
    public void AuditLog(string tableName, string RowID, string ColName, string OldData, string NewData, string UserID) {
        DataManipulation("insert into AuditLog values('" + tableName + "','" + RowID + "','" + ColName + "','" + OldData + "','" + NewData + "','" + UserID + "',GETDATE())");
    }
    #endregion

    #region BulkUploads
    /// <summary>
    /// Using this method we get all table in which we upload the records.
    /// </summary>
    /// <returns></returns>
    public DataTable getAllBulkUploadTables()
    {
        cmd = new SqlCommand("USP_SelectBulkUploadTables", con);
        cmd.CommandType = CommandType.StoredProcedure;
        return GetDataTable(cmd);
    }
    /// <summary>
    /// Using this method we get records with data or without data.
    /// </summary>
    /// <param name="tableName"> Selected Table Name</param>
    /// <param name="typeOfQuery"> Type of query with data or without data</param>
    /// <returns></returns>
    public DataTable getExcelFormetOfSelectedTable(string tableName, string typeOfQuery)
    {
        cmd = new SqlCommand("USP_GetExcelFormatOfTable", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@TableName", tableName);
        cmd.Parameters.AddWithValue("@TypeOfQuery", typeOfQuery);

        return GetDataTable(cmd);
    }
    /// <summary>
    /// Using this method we get the column details of tables.
    /// </summary>
    /// <param name="tableName">Selected Table Name</param>
    /// <returns></returns>
    public DataTable getTableCloumnDetail(string tableName)
    {
        cmd = new SqlCommand("USP_GetTableColumnDetails", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@TableName", tableName);
        return GetDataTable(cmd);
    }
    /// <summary>
    /// Creating a table of copy at database.
    /// </summary>
    /// <param name="tableName"></param>
    public void createTableCopy(string tableName)
    {
        cmd = new SqlCommand("USP_CreateTableCopy", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@TableName", tableName);
       
        try
        {
            cmd.CommandTimeout = 200;
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            con.Close();
        }

        //  return GetDataTable(cmd);
    }

    /// <summary>
    /// Using this method we upload a bulk records in Database.
    /// </summary>
    /// <param name="source">Data table source</param>
    /// <param name="destination">Dsetination of Table Name.</param>
    public void BulkUpload(DataTable source, string destination) {
        SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.TableLock, null);
        oSqlBulkCopy.DestinationTableName = destination;
        foreach (DataColumn column in source.Columns) {
            oSqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());
        }
        con.Open();
        oSqlBulkCopy.BulkCopyTimeout = 300;
        oSqlBulkCopy.BatchSize = source.Rows.Count;
        oSqlBulkCopy.WriteToServer(source);
        oSqlBulkCopy.Close();
        con.Close();
    }
    /// <summary>
    /// Copy data table from staging to uploaded table.
    /// </summary>
    /// <param name="tableName"></param>
    public void CopyDataFromStagingToUpld(string tableName) {
        cmd = new SqlCommand("USP_CopyDataFromStagingToUpld", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@TableName", tableName);
       
        try {
            cmd.CommandTimeout = 200;
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (Exception) {
            throw;
        }
        finally {
            con.Close();
        }

        //  return GetDataTable(cmd);
    }
    /// <summary>
    /// Using this method we delete the staging table.
    /// </summary>
    public void DeleteStagingTable() {
        if (con.State != ConnectionState.Closed)
            con.Close();
        cmd = new SqlCommand("USP_DeleteStagingTable", con);
        cmd.CommandType = CommandType.StoredProcedure;
      
        try {
            cmd.CommandTimeout = 200;
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (Exception) {
            throw;
        }
        finally {
            con.Close();
        }
    }

    #endregion
}